package com.finance.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComboBox;
import javax.swing.text.MaskFormatter;

import com.finance.gui.dialogs.models.DocumentComboBoxModel;
import com.finance.model.Document;

public class Utilities 
{
	public static boolean isInteger(String string)
	{
		try
		{
			int x = Integer.parseInt(string);
		}
		catch(NumberFormatException e)
		{
			return false;
		}
		return true;
	}
	
	public static boolean isDouble(String string)
	{
		try
		{
			double x = Double.parseDouble(string);
		}
		catch(NumberFormatException e)
		{
			return false;
		}
		return true;
	}
	
	
	public static boolean isDate(String string)
	{
		DateFormat format = new SimpleDateFormat("dd/mm/yyyy");
		try
		{
			Date d = (Date) format.parse(string);
		}
		catch(ParseException ex)
		{
			return false;
		}
		return true;
	}
	
	public static boolean isClientCode(String string)
	{
		try
		{
			 MaskFormatter f = new MaskFormatter("30.##.##.##");
			 f.valueToString(string);
		}
		catch(ParseException ex)
		{
			return false;
		}
		return true;
	}
	
	public static boolean isSupplierCode(String string)
	{
		try
		{
			 MaskFormatter f = new MaskFormatter("50.##.##.##");
			 f.valueToString(string);
		}
		catch(ParseException ex)
		{
			return false;
		}
		return true;
	}
	
	public static boolean isCode(String string)
	{
		boolean matches = false;
		try
		{
			String code = "\\d{2}.\\d{2}.\\d{2}.\\d{2}";
			Pattern pattern = Pattern.compile(code);
		    Matcher matcher = pattern.matcher(string);
		    matches = matcher.matches();
		}
		catch(Exception ex)
		{
			return false;
		}
		return matches;
	}
	
	public static boolean isPhone(String string)
	{
		boolean matches = false;
		try
		{
			String code = "\\+\\d{2}\\-\\d{10}";
			Pattern pattern = Pattern.compile(code);
		    Matcher matcher = pattern.matcher(string);
		    matches = matcher.matches();
		}
		catch(Exception ex)
		{
			return false;
		}
		return matches;
	}
	
	
	public static String parseDate(String dateString)
	{
		String oldFormat = "dd/MM/yyyy";
		String newFormat = "yyyy-MM-dd";
		
		SimpleDateFormat sdf = new SimpleDateFormat(oldFormat); 
		try 
		{
			Date date = sdf.parse(dateString);
			sdf.applyPattern(newFormat);
			return sdf.format(date);
		} 
		catch (ParseException e) 
		{

		}
		return null;
	}
	
	public static String parseReverseDate(String dateString)
	{
		String oldFormat = "yyyy-MM-dd";
		String newFormat = "dd/MM/yyyy";
		
		SimpleDateFormat sdf = new SimpleDateFormat(oldFormat); 
		try 
		{
			Date date = sdf.parse(dateString);
			sdf.applyPattern(newFormat);
			return sdf.format(date);
		} 
		catch (ParseException e) 
		{

		}
		return null;
	}
	
	
	
	public static boolean isYear(String year)
	{
		int y = 0;
		try
		{
			y = Integer.parseInt(year);
		}
		catch(NumberFormatException e)
		{
			return false;
		}
		return (y >= 1900 && y <=2100) ? true : false;
	}
}
