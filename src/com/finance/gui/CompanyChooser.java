package com.finance.gui;


import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.finance.controller.CompanyController;
import com.finance.gui.dialogs.CreateCompanyDialog;
import net.miginfocom.swing.MigLayout;

public class CompanyChooser extends JFrame 
{
	private JFileChooser _fileChooser;
	
	private JRadioButton _createCompany;
	private JRadioButton _selectCompany;
	
	private JButton _confirmButton;
	private JButton _cancelButton;
	
	public CompanyChooser()
	{
		super("");
		initGUI();
	}

	private void initGUI() 
	{
		Icon icon = new ImageIcon( getClass().getResource( "icon.jpg" ) );
		
		JOptionPane.showMessageDialog(CompanyChooser.this,"��� ��� ������ ��� ��������� ������ �� "
				+ "��������� ��� ��� ��� ���������� �������� � �� ������������� ��� ����������",
			    "����������� ���� �������� finance_app",
			    JOptionPane.INFORMATION_MESSAGE,
			    icon);
		
		this.setLocationRelativeTo(null);
		this.setSize(240, 140);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		JPanel buttonPanel = new JPanel(new GridLayout());
		
		_createCompany = new JRadioButton("���������� ��������");
		fieldsPanel.add(_createCompany,"wrap");
		
		_selectCompany = new JRadioButton("������� ��������");
		fieldsPanel.add(_selectCompany);
		
		ButtonGroup radioGroup = new ButtonGroup();
		radioGroup.add(_createCompany);
		radioGroup.add(_selectCompany);
		
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(_createCompany.isSelected())
				{
					CreateCompanyDialog dlg = new CreateCompanyDialog(CompanyChooser.this);
					dlg.display();
					if(dlg.hasCompany())
					{
						MainForm form = new MainForm(dlg.getCompanyName());
						form.setVisible(true);
						setVisible(false);
					}
				}
				else if(_selectCompany.isSelected()) 
				{

			    	_fileChooser = new JFileChooser();
			    	_fileChooser.setAcceptAllFileFilterUsed(false);
			    	_fileChooser.setFileFilter(new FileNameExtensionFilter("db","db"));
			    	int ret = _fileChooser.showOpenDialog(null);
			    	if(ret == JFileChooser.APPROVE_OPTION)
			    	{
			    		String companyName = _fileChooser.getSelectedFile().getName();
			    		MainForm form = new MainForm(companyName.substring(0, companyName.length() - 3));
			    		String dbPath = _fileChooser.getSelectedFile().getAbsolutePath();
			    		CompanyController.selectCompanyDB(dbPath);
			    		form.setVisible(true);
			    		setVisible(false);
			    	}
				}
				else
					JOptionPane.showMessageDialog(CompanyChooser.this, "������ �� ��������� ��� ��� �� ��� �����",  "������", JOptionPane.ERROR_MESSAGE);
			}
			
		});
		buttonPanel.add(_confirmButton);
		
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				System.exit(0);
			}
			
		});
		buttonPanel.add(_cancelButton);
		
		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(dialogPanel);
	
	}

}
