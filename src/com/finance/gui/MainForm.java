package com.finance.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.finance.controller.CategoryIncomeController;
import com.finance.controller.CategoryOutcomeController;
import com.finance.controller.ClientController;
import com.finance.controller.DocumentIncomeController;
import com.finance.controller.DocumentOutcomeController;
import com.finance.controller.IncomeTransactionController;
import com.finance.controller.OutcomeTranstactionController;
import com.finance.controller.SupplierController;
import com.finance.gui.dialogs.CreateCategoryIncomeDialog;
import com.finance.gui.dialogs.CreateCategoryOutcomeDialog;
import com.finance.gui.dialogs.CreateClientDialog;
import com.finance.gui.dialogs.CreateDocumentIncomeDialog;
import com.finance.gui.dialogs.CreateDocumentOutcomeDialog;
import com.finance.gui.dialogs.CreateSupplierDialog;
import com.finance.gui.dialogs.CreateTransactionIncomeDialog;
import com.finance.gui.dialogs.CreateTransactionOutcomeDialog;
import com.finance.gui.dialogs.DeleteIncomeTransaction;
import com.finance.gui.dialogs.DeleteOutcomeTransaction;
import com.finance.gui.dialogs.EditCategoryIncomeDialog;
import com.finance.gui.dialogs.EditCategoryOutcomeDialog;
import com.finance.gui.dialogs.EditClientDialog;
import com.finance.gui.dialogs.EditCompanyDialog;
import com.finance.gui.dialogs.EditDocumentIncomeDialog;
import com.finance.gui.dialogs.EditDocumentOutcomeDialog;
import com.finance.gui.dialogs.EditSupplierDialog;
import com.finance.gui.dialogs.TaxDialog;
import com.finance.gui.dialogs.VATForYieldDialog;
import com.finance.model.Account;
import com.finance.model.Category;
import com.finance.model.Document;
import com.finance.model.Transaction;
import com.finance.model.TransactionType;

public class MainForm extends JFrame 
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -8565851213376028375L;

	private JMenuBar _menuBar;
	private JPanel _mainPanel;
	private Preferences prefs;
	private double[] _taxes;
	
	private List<TransactionType> _incomeTypes;
	private List<TransactionType> _outcomeTypes;
	
	public MainForm(String companyName)
    {
        super("������� - " + companyName);
        // This will define a node in which the preferences can be stored
        prefs = Preferences.userRoot().node(this.getClass().getName());
        _taxes = new double[3];
        _taxes[0] = prefs.getDouble("smallTax", 13.0); //default small 13%
        _taxes[1] = prefs.getDouble("largeTax", 23.0); //default large 23%
        _taxes[2] = prefs.getDouble("nonTax"  , 0.0);  
        initTranscationTypes();
        initGUI();
    }    
	
    private void initGUI()
    {
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	this.setLocationRelativeTo(null);
    	setSize(600, 400);
    	//this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    	initMenu();
    	initLayout();
    }
    
    private void initLayout()
    {
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(_menuBar, BorderLayout.NORTH);
    }
    
    
    
    private void initMenu()
    {
        _menuBar = new JMenuBar();
        //create menu
        JMenu companyMenu = new JMenu("�������");
        JMenu accountMenu = new JMenu("�������-�����������");
        JMenu incomeOutcomeMenu = new JMenu("�����-�����");
        JMenu documentMenu = new JMenu("����������");
        JMenu categoryMenu = new JMenu("�����������");
        JMenu resultMenu = new JMenu("������������");
        JMenu settingsMenu = new JMenu("���������");
        
        //company menu
        JMenuItem editCompany = new JMenuItem("����������� ��������");
        
        editCompany.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				EditCompanyDialog dlg = new EditCompanyDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        companyMenu.add(editCompany);
        _menuBar.add(companyMenu);
        
      //accountMenu menu
        JMenuItem createClient = new JMenuItem("���������� ������");
        JMenuItem createSupplier = new JMenuItem("���������� ����������");
        JMenuItem editClient= new JMenuItem("����������� ������");
        JMenuItem editSupplier= new JMenuItem("����������� ����������");
        JMenuItem displayClient = new JMenuItem("������� ������");
        JMenuItem displaySupplier = new JMenuItem("������� ����������");
        
        createClient.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				CreateClientDialog dlg = new CreateClientDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        createSupplier.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				CreateSupplierDialog dlg = new CreateSupplierDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        editClient.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				EditClientDialog dlg = new EditClientDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        editSupplier.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				EditSupplierDialog dlg = new EditSupplierDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        displayClient.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(_mainPanel != null) getContentPane().remove(_mainPanel);
				ClientController controller = new ClientController();
				List<Account> accounts = controller.getAll();
				if(accounts.isEmpty())
				{
					JOptionPane.showMessageDialog(MainForm.this, "��� �������� �������� �������",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				String[] columns = {"�/�", "�������", "�������������", "���������", "���������", "����", "�.�.", "�.�.�.", "�.�.�.", "���������", "��������"};
				String[][] fields = new String[accounts.size()][columns.length];
				int i = 0;
				for(Account account : accounts)
				{
					fields[i] = account.getFields();
					i++;
				}
				initJTable(columns, fields);
			}
        	
        });
        
        displaySupplier.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(_mainPanel != null) getContentPane().remove(_mainPanel);
				SupplierController controller = new SupplierController();
				List<Account> accounts = controller.getAll();
				if(accounts.isEmpty())
				{
					JOptionPane.showMessageDialog(MainForm.this, "��� �������� �������� �����������",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				String[] columns = {"�/�", "�������", "�������������", "���������", "���������", "����", "�.�.", "�.�.�.", "�.�.�.", "���������", "��������"};
				String[][] fields = new String[accounts.size()][columns.length];
				int i = 0;
				for(Account account : accounts)
				{
					fields[i] = account.getFields();
					i++;
				}
				initJTable(columns, fields);
			}
        	
        });
        
        accountMenu.add(createClient);
        accountMenu.add(createSupplier);
        accountMenu.add(editClient);
        accountMenu.add(editSupplier);
        accountMenu.add(displayClient);
        accountMenu.add(displaySupplier);
        _menuBar.add(accountMenu);
      
        
        //incomeOutcome menu
        JMenuItem createIncome = new JMenuItem("���������� ������");
        JMenuItem createOutcome = new JMenuItem("���������� ������");
        JMenuItem deleteIncome = new JMenuItem("�������� ������� ������");
        JMenuItem deleteOutcome = new JMenuItem("�������� ������� ������");
        JMenuItem displayIncome = new JMenuItem("������� ������");
        JMenuItem displayOutcome = new JMenuItem("������� ������");
        
        createIncome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				CreateTransactionIncomeDialog in = new CreateTransactionIncomeDialog(_taxes);
				in.setVisible(true);
			}
        	
        });
        
        createOutcome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				CreateTransactionOutcomeDialog out = new CreateTransactionOutcomeDialog(_taxes);
				out.setVisible(true);
			}
        	
        });
        
        deleteIncome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				DeleteIncomeTransaction delIncome = new DeleteIncomeTransaction();
				delIncome.setVisible(true);
			}
        	
        });
        
        deleteOutcome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				DeleteOutcomeTransaction delOutcome = new DeleteOutcomeTransaction();
				delOutcome.setVisible(true);
			}
        	
        });
        
        displayIncome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(_mainPanel != null) getContentPane().remove(_mainPanel);
				IncomeTransactionController controller = new IncomeTransactionController();
				List<Transaction> transactions = controller.getAll();
				if(transactions.isEmpty())
				{
					JOptionPane.showMessageDialog(MainForm.this, "��� �������� �������� ���������� ������",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				String[] columns = {"�/�", "�����", "����������", "���������", "�����������", "����"};
				String[][] fields = new String[transactions.size()][columns.length];
				int i = 0;
				for(Transaction transaction : transactions)
				{
					fields[i] = transaction.getFields();
					//fields[1] -> type 
					int type = Integer.parseInt(fields[i][1]);
					fields[i][1] = _incomeTypes.get(type - 1).toString();
					i++;
				}
				initJTable(columns, fields);
			}
        	
        });
        
        displayOutcome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(_mainPanel != null) getContentPane().remove(_mainPanel);
				OutcomeTranstactionController controller = new OutcomeTranstactionController();
				List<Transaction> transactions = controller.getAll();
				if(transactions.isEmpty())
				{
					JOptionPane.showMessageDialog(MainForm.this, "��� �������� �������� ���������� ������",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				String[] columns = {"�/�", "�����", "����������", "���������", "�����������", "����"};
				String[][] fields = new String[transactions.size()][columns.length];
				int i = 0;
				for(Transaction transaction : transactions)
				{
					fields[i] = transaction.getFields();
					//fields[1] -> type 
					int type = Integer.parseInt(fields[i][1]);
					fields[i][1] = _outcomeTypes.get(type - 1).toString();
					i++;
				}
				initJTable(columns, fields);
			}
        	
        });
        
        incomeOutcomeMenu.add(createIncome);
        incomeOutcomeMenu.add(createOutcome);
        incomeOutcomeMenu.add(deleteIncome);
        incomeOutcomeMenu.add(deleteOutcome);
        incomeOutcomeMenu.add(displayIncome);
        incomeOutcomeMenu.add(displayOutcome);
        _menuBar.add(incomeOutcomeMenu);
        
        
        //documentCategoryMenu menu
        JMenuItem addDocumentIncome = new JMenuItem("�������� ������������ ������");
        JMenuItem addDocumentOutcome = new JMenuItem("�������� ������������ ������");
        JMenuItem editDocumentIncome = new JMenuItem("����������� ������������ ������");
        JMenuItem editDocumentOutcome = new JMenuItem("����������� ������������ ������");
        JMenuItem displayDocumentIncome = new JMenuItem("������� ������������ ������");
        JMenuItem displayDocumentOutcome = new JMenuItem("������� ������������ ������");
        
        
        addDocumentIncome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				CreateDocumentIncomeDialog dlg = new CreateDocumentIncomeDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        addDocumentOutcome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				CreateDocumentOutcomeDialog dlg = new CreateDocumentOutcomeDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        
        
        editDocumentIncome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				EditDocumentIncomeDialog dlg = new EditDocumentIncomeDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        editDocumentOutcome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				EditDocumentOutcomeDialog dlg = new EditDocumentOutcomeDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        displayDocumentIncome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(_mainPanel != null) getContentPane().remove(_mainPanel);
				DocumentIncomeController controller = new DocumentIncomeController();
				List<Document> documents = controller.getAll();
				if(documents.isEmpty())
				{
					JOptionPane.showMessageDialog(MainForm.this, "��� �������� �������� ������������ ������",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				String[] columns = {"�/�", "�����", "�������������", "�������� �.�.�."};
				String[][] fields = new String[documents.size()][columns.length];
				int i = 0;
				for(Document document : documents)
				{
					fields[i] = document.getFields();
					i++;
				}
				initJTable(columns, fields);
			}
        	
        });
        
        displayDocumentOutcome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(_mainPanel != null) getContentPane().remove(_mainPanel);
				DocumentOutcomeController controller = new DocumentOutcomeController();
				List<Document> documents = controller.getAll();
				if(documents.isEmpty())
				{
					JOptionPane.showMessageDialog(MainForm.this, "��� �������� �������� ������������ ������",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				String[] columns = {"�/�", "�����", "�������������", "�������� �.�.�."};
				String[][] fields = new String[documents.size()][columns.length];
				int i = 0;
				for(Document document : documents)
				{
					fields[i] = document.getFields();
					i++;
				}
				initJTable(columns, fields);
			}
        	
        });
        
       
        
        documentMenu.add(addDocumentIncome);
        documentMenu.add(addDocumentOutcome);
        documentMenu.add(editDocumentIncome);
        documentMenu.add(editDocumentOutcome);
        documentMenu.add(displayDocumentIncome);
        documentMenu.add(displayDocumentOutcome);
        _menuBar.add(documentMenu);
        
       // CategoryMenu menu
        JMenuItem addCategoryIncome = new JMenuItem("�������� ����������� ������");
        JMenuItem addCategoryOutcome = new JMenuItem("�������� ����������� ������");
        JMenuItem editCategoryIncome = new JMenuItem("����������� ����������� ������");
        JMenuItem editCategoryOutcome = new JMenuItem("����������� ����������� ������");
        JMenuItem displayCategoryIncome = new JMenuItem("������� ����������� ������");
        JMenuItem displayCategoryOutcome = new JMenuItem("������� ����������� ������");
        
        addCategoryIncome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				CreateCategoryIncomeDialog dlg = new CreateCategoryIncomeDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        addCategoryOutcome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				CreateCategoryOutcomeDialog dlg = new CreateCategoryOutcomeDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        editCategoryIncome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				EditCategoryIncomeDialog dlg = new EditCategoryIncomeDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        editCategoryOutcome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				EditCategoryOutcomeDialog dlg = new EditCategoryOutcomeDialog();
				dlg.setVisible(true);
			}
        	
        });
        
        displayCategoryIncome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(_mainPanel != null) getContentPane().remove(_mainPanel);
				CategoryIncomeController controller = new CategoryIncomeController();
				List<Category> categories = controller.getAll();
				if(categories.isEmpty())
				{
					JOptionPane.showMessageDialog(MainForm.this, "��� �������� �������� ����������� ������",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				String[] columns = {"�/�", "�������", "����������", "������� ������"};
				String[][] fields = new String[categories.size()][columns.length];
				int i = 0;
				for(Category category : categories)
				{
					fields[i] = category.getFields();
					i++;
				}
				initJTable(columns, fields);
			}
        	
        });
        
        displayCategoryOutcome.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(_mainPanel != null) getContentPane().remove(_mainPanel);
				CategoryOutcomeController controller = new CategoryOutcomeController();
				List<Category> categories = controller.getAll();
				if(categories.isEmpty())
				{
					JOptionPane.showMessageDialog(MainForm.this, "��� �������� �������� ����������� ������",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				String[] columns = {"�/�", "�������", "����������", "������� ������"};
				String[][] fields = new String[categories.size()][columns.length];
				int i = 0;
				for(Category category : categories)
				{
					fields[i] = category.getFields();
					i++;
				}
				initJTable(columns, fields);
			}
        	
        });
        
        categoryMenu.add(addCategoryIncome);
        categoryMenu.add(addCategoryOutcome);
        categoryMenu.add(editCategoryIncome);
        categoryMenu.add(editCategoryOutcome);
        categoryMenu.add(displayCategoryIncome);
        categoryMenu.add(displayCategoryOutcome);
        _menuBar.add(categoryMenu);
        
        //resultMenu menu
        JMenuItem displayResult = new JMenuItem("�������� �������������");
   
        displayResult.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				VATForYieldDialog vat = new VATForYieldDialog(_taxes);
				vat.setVisible(true);
			}
        	
        });
        
        resultMenu.add(displayResult);
        _menuBar.add(resultMenu);
        
        JMenuItem taxMenuItem = new JMenuItem("����������� �.�.�.");
        taxMenuItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
                TaxDialog dialog = new TaxDialog(MainForm.this, _taxes);
                dialog.display();
                _taxes = dialog.getTaxes();
                prefs.putDouble("smallTax", _taxes[0]);
                prefs.putDouble("largeTax", _taxes[1]);
                initTranscationTypes();
			}
        });
        
        settingsMenu.add(taxMenuItem);
        _menuBar.add(settingsMenu);
       
    }
    
   
    private void initJTable(String[] columns, String[][] fields)
    {
    	_mainPanel = new JPanel();
    	_mainPanel.setLayout(new BoxLayout(_mainPanel, BoxLayout.LINE_AXIS));
    	JTable table = new JTable(new DefaultTableModel(fields, columns));
    	JScrollPane tableScrollPanel = new JScrollPane(table);
    	_mainPanel.add(tableScrollPanel);
    	getContentPane().add(_mainPanel);
    	this.revalidate();
    	this.repaint();
    }
    
    
    private void initTranscationTypes()
    {
    	_incomeTypes = new ArrayList<>();
    	_incomeTypes.add(new TransactionType(1, "��������� ��������", _taxes[0]));
    	_incomeTypes.add(new TransactionType(2, "��������� ��������", _taxes[1]));
    	_incomeTypes.add(new TransactionType(3, "�������� ��������", _taxes[0]));
    	_incomeTypes.add(new TransactionType(4, "�������� ��������", _taxes[1]));
    	_incomeTypes.add(new TransactionType(5, "�������� ������", _taxes[1]));
    	_incomeTypes.add(new TransactionType(6, "����� �����", _taxes[1]));
    	
    	_outcomeTypes = new ArrayList<>();
    	_outcomeTypes.add(new TransactionType(1, "������", _taxes[0]));
    	_outcomeTypes.add(new TransactionType(2, "������", _taxes[1]));
    	_outcomeTypes.add(new TransactionType(3, "������� �� �������� ��������", _taxes[0]));
    	_outcomeTypes.add(new TransactionType(4, "������� ����� �������� ��������", _taxes[2]));
    	_outcomeTypes.add(new TransactionType(5, "�����", _taxes[1]));
    	_outcomeTypes.add(new TransactionType(6, "����� �����", _taxes[1]));
    }
    
    public static void main(String[] args)
    {
    	CompanyChooser cc = new CompanyChooser();
    	cc.setVisible(true);
    }

	
	
}
