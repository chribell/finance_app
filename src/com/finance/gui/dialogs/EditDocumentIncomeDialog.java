package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.controller.DocumentIncomeController;
import com.finance.model.Category;
import com.finance.model.Document;
import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class EditDocumentIncomeDialog extends JFrame
{
	private Document _document;
	
	private JTextField _searchAbbreviation;
	private JTextField _name;
	private JTextField _abbreviation; 
	private JCheckBox _requirementRegistryNumber;
	
	private JButton _searchButton;
	private JButton _confirmButton;
	private JButton _cancelButton;
	
	private DocumentIncomeController _controller;
	public EditDocumentIncomeDialog()
	{
		super("����������� ������������ ������");
		_controller = new DocumentIncomeController();
		initGUI();
	}
	
	private void initGUI()
	{
		this.setLocationRelativeTo(null);
		this.setSize(450, 200);
		JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		JPanel searchPanel = new JPanel(new FlowLayout());
		
		searchPanel.add(new JLabel("�������������"));
		_searchAbbreviation = new JTextField(20);
		searchPanel.add(_searchAbbreviation);
		
		_searchButton = new JButton("���������");
		_searchButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String searchAbbreviation = _searchAbbreviation.getText().toString();
				if(searchAbbreviation.isEmpty())
				{
					JOptionPane.showMessageDialog(EditDocumentIncomeDialog.this, "������ �� �������� �������������",  "��������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				_document = _controller.getDocumentIncomeByAbbreviation(searchAbbreviation);
				if(_document != null)
				{
					enableFields();
					setFieldsValues();
				}
				else
				{
					JOptionPane.showMessageDialog(EditDocumentIncomeDialog.this, "������ �� �������� �������������",  "��������", JOptionPane.ERROR_MESSAGE);
					disableFields();
				}
			}
		});
		searchPanel.add(_searchButton);
		
		fieldsPanel.add(new JLabel("�����"));
		_name = new JTextField(20);
		fieldsPanel.add(_name, "wrap");
		
		fieldsPanel.add(new JLabel("�������������"));
		_abbreviation = new JTextField(20);
		fieldsPanel.add(_abbreviation, "wrap");
		
		fieldsPanel.add(new JLabel("�������� �.�.�."));
		_requirementRegistryNumber = new JCheckBox();
		fieldsPanel.add(_requirementRegistryNumber);
		
		
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(checkFields())
				{
					int rRNNum = _requirementRegistryNumber.isSelected() ? 1 : 0;
					_document = new Document(_document.get_id(),
											_name.getText().toString(),
											_abbreviation.getText().toString(),
											rRNNum											 	 
											);
					if(_controller.updateDocumentIncome(_document))
						JOptionPane.showMessageDialog(EditDocumentIncomeDialog.this, "�������� ������ ������������ ������",  "���������", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(EditDocumentIncomeDialog.this, "���������� ������ ������������ ������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(EditDocumentIncomeDialog.this, WindowEvent.WINDOW_CLOSING));
				}
			}
			
		});
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				dispatchEvent(new WindowEvent(EditDocumentIncomeDialog.this, WindowEvent.WINDOW_CLOSING));
				
			}
			
		});
		
		buttonPanel.add(_confirmButton);
		buttonPanel.add(_cancelButton);
		
		dialogPanel.add(searchPanel, BorderLayout.NORTH);
		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(dialogPanel);
		disableFields();
	}
	
	private boolean checkFields()
	{
		String name = _name.getText().toString();
		if(name.length() != 11)
		{
			JOptionPane.showMessageDialog(EditDocumentIncomeDialog.this, "������ �� �������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String abbreviation = _abbreviation.getText().toString();
		if(abbreviation.isEmpty())
		{
			JOptionPane.showMessageDialog(EditDocumentIncomeDialog.this, "������ �� �������� �������������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String requirementRegistryNumber = _requirementRegistryNumber.getText().toString();
		if(requirementRegistryNumber.isEmpty())
		{
			JOptionPane.showMessageDialog(EditDocumentIncomeDialog.this, "������ �� �������� �������� �.�.�.",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isInteger(requirementRegistryNumber))
		{
			JOptionPane.showMessageDialog(EditDocumentIncomeDialog.this, "� �������� �.�.�. ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	private void disableFields()
	{
		_name.setEnabled(false);
		_abbreviation.setEnabled(false);
		_requirementRegistryNumber.setEnabled(false);

		_confirmButton.setEnabled(false);
	}
	
	private void enableFields()
	{
		_name.setEnabled(true);
		_requirementRegistryNumber.setEnabled(true);

		_confirmButton.setEnabled(true);
	}
	
	private void setFieldsValues()
	{
		_name.setText((_document.get_name()));
		_requirementRegistryNumber.setText(_document.get_requirementRegistryNumber() + "");
	}
}
