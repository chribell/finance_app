package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.controller.ClientController;
import com.finance.model.Account;
import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class CreateClientDialog extends JFrame {
	
	private JTextField _code;
	private JTextField _name;
	private JTextField _inCharge;
	private JTextField _occupation;
	private JTextField _city;
	private JTextField _zipCode;
	private JTextField _registryNumber;
	private JTextField _taxStore;
	private JTextField _address;
	private JTextField _phone;
	
	private JButton _confirmButton;
	private JButton _cancelButton;
	
	private ClientController _controller;
	
	public CreateClientDialog()
	{
		super("���������� ������");
		_controller = new ClientController();
		initGUI();
	}
	
	private void initGUI() 
	{
		
		this.setLocationRelativeTo(null);
		this.setSize(500, 300);
		JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		
		fieldsPanel.add(new JLabel("�������"));
		_code = new JTextField(20);
		fieldsPanel.add(_code);
		
		fieldsPanel.add(new JLabel("������������� / �������"));
		_name = new JTextField(20);
		fieldsPanel.add(_name, "wrap");
		
		fieldsPanel.add(new JLabel("���������"));
		_inCharge = new JTextField(20);
		fieldsPanel.add(_inCharge);
		
		fieldsPanel.add(new JLabel("���������"));
		_occupation = new JTextField(20);
		fieldsPanel.add(_occupation, "wrap");
		
		fieldsPanel.add(new JLabel("����"));
		_city = new JTextField(20);
		fieldsPanel.add(_city);
		
		fieldsPanel.add(new JLabel("������������ �������"));
		_zipCode = new JTextField(20);
		fieldsPanel.add(_zipCode, "wrap");		
		
		fieldsPanel.add(new JLabel("�.�.�."));
		_registryNumber = new JTextField(20);
		fieldsPanel.add(_registryNumber);
		
		fieldsPanel.add(new JLabel("�.�.�."));
		_taxStore = new JTextField(20);
		fieldsPanel.add(_taxStore, "wrap");
		
		fieldsPanel.add(new JLabel("���������"));
		_address = new JTextField(20);
		fieldsPanel.add(_address);
		
		fieldsPanel.add(new JLabel("��������"));
		_phone = new JTextField(20);
		fieldsPanel.add(_phone, "wrap");
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{	
				if(checkFields())
				{
					Account account = new Account(_controller.getNumberOfClients() + 1,
	                                              _code.getText().toString(),
	                                              _name.getText().toString(),
	                                              _inCharge.getText().toString(),
	                                              _occupation.getText().toString(),
	                                              _city.getText().toString(),	
	                                              _zipCode.getText().toString(),
	                                              _registryNumber.getText().toString(),
	                                              _taxStore.getText().toString(),
	                                              _address.getText().toString(),
	                                              _phone.getText().toString()  
												);
					if(_controller.createClient(account))
						JOptionPane.showMessageDialog(CreateClientDialog.this, "�������� ���������� ������",  "���������", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(CreateClientDialog.this, "���������� ���������� ������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(CreateClientDialog.this, WindowEvent.WINDOW_CLOSING));
				}
			}
		});
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				dispatchEvent(new WindowEvent(CreateClientDialog.this, WindowEvent.WINDOW_CLOSING));
				
			}
			
		});
		
		buttonPanel.add(_confirmButton);
		buttonPanel.add(_cancelButton);
		
		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(dialogPanel);
		
	}
	
	private boolean checkFields()
	{
		String code = _code.getText().toString();
		if(!Utilities.isClientCode(code))
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "������ �� �������� ������ ��� ������ 50.��.��.��",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String name = _name.getText().toString();
		if(name.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "������ �� �������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String inCharge = _inCharge.getText().toString();
		if(inCharge.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String occupation = _occupation.getText().toString();
		if(occupation.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "������ �� �������� ���������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String city = _city.getText().toString();
		if(city.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "������ �� �������� ����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String zipCode = _zipCode.getText().toString();
		if(zipCode.length() != 5)
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "������ �� �������� ����������� ������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isInteger(zipCode))
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "� ������������ ������� ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String registryNumber = _registryNumber.getText().toString();
		if(registryNumber.length() != 9)
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "�� ��� ������ �� ����� 9�����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if(!Utilities.isInteger(registryNumber))
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "�� ��� ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String taxStore = _taxStore.getText().toString();
		if(taxStore.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "������ �� �������� �.�.�.",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String address = _address.getText().toString();
		if(address.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "������ �� �������� ���������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String phone = _phone.getText().toString();
		if(phone.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if(!Utilities.isPhone(phone))
		{
			JOptionPane.showMessageDialog(CreateClientDialog.this, "�� �������� ������ �� ����� ��� ������ +30-����������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}

}
