package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.controller.CategoryIncomeController;
import com.finance.model.Category;
import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class CreateCategoryIncomeDialog extends JFrame
{	
	private JTextField _code;
	private JTextField _explanation; 
	private JTextField _field;
	
	
	private JButton _confirmButton;
	private JButton _cancelButton;
	
	private CategoryIncomeController _controller;
	
	public CreateCategoryIncomeDialog()
	{
		super("�������� ����������� ������");
		_controller = new  CategoryIncomeController();
		initGUI();
	}
	
	private void initGUI()
	{
		this.setLocationRelativeTo(null);
		this.setSize(400, 200);
		JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		
		fieldsPanel.add(new JLabel("�������"));
		_code = new JTextField(20);
		fieldsPanel.add(_code, "wrap");
		
		fieldsPanel.add(new JLabel("���������"));
		_explanation = new JTextField(20);
		fieldsPanel.add(_explanation, "wrap");
		
		fieldsPanel.add(new JLabel("�����"));
		_field = new JTextField(20);
		fieldsPanel.add(_field);
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(checkFields())
				{
					int fieldNum = Integer.parseInt(_field.getText().toString());
					Category category = new Category(_controller.getNumberOfIncomeCategories() + 1,
												  _code.getText().toString(),
												  _explanation.getText().toString(),
												  fieldNum											 	 
												);
					if(_controller.createCategoryIncome(category))
						JOptionPane.showMessageDialog(CreateCategoryIncomeDialog.this, "�������� ���������� ����������",  "���������", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(CreateCategoryIncomeDialog.this, "���������� ���������� ����������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(CreateCategoryIncomeDialog.this, WindowEvent.WINDOW_CLOSING));
				}
				
			}
			
		});
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				dispatchEvent(new WindowEvent(CreateCategoryIncomeDialog.this, WindowEvent.WINDOW_CLOSING));
				
			}
			
		});
		
		buttonPanel.add(_confirmButton);
		buttonPanel.add(_cancelButton);
		
		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(dialogPanel);
	}
	
	private boolean checkFields()
	{
		String code = _code.getText().toString();
		if(!Utilities.isCode(code))
		{
			JOptionPane.showMessageDialog(CreateCategoryIncomeDialog.this, "������ �� �������� ������ ��� ������ nn.nn.nn.nn",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String explanation = _explanation.getText().toString();
		if(explanation.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateCategoryIncomeDialog.this, "������ �� �������� ���������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String field = _field.getText().toString();
		if(field.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateCategoryIncomeDialog.this, "������ �� �������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isInteger(field))
		{
			JOptionPane.showMessageDialog(CreateCategoryIncomeDialog.this, "�� ����� ������ �� ��������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
}
