package com.finance.gui.dialogs.models;

import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import com.finance.model.TransactionType;


public class TransactionTypeComboBoxModel extends AbstractListModel implements ComboBoxModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2605620754202722577L;

	private List<TransactionType> typeList;
	
	private TransactionType selection = null;
	
	public TransactionTypeComboBoxModel(List<TransactionType> typeList) {
		this.typeList = typeList;
	}

	@Override
	public Object getElementAt(int arg0) {
		// TODO Auto-generated method stub
		return typeList.get(arg0);
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return typeList.size();
	}

	@Override
	public Object getSelectedItem() {
		// TODO Auto-generated method stub
		return selection;
	}

	@Override
	public void setSelectedItem(Object arg0) {
		// TODO Auto-generated method stub
		selection = (TransactionType) arg0;
	}

}
