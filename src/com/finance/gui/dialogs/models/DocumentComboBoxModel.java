package com.finance.gui.dialogs.models;

import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import com.finance.model.Document;

public class DocumentComboBoxModel extends AbstractListModel implements ComboBoxModel 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3189622355272944038L;
	private List<Document> _documentList;
	
	private Document _selection = null;
	
	public DocumentComboBoxModel(List<Document> documentList) 
	{
		_documentList = documentList;
	}
	
	@Override
	public Object getElementAt(int index) 
	{
		return _documentList.get(index);
	}

	@Override
	public int getSize() 
	{
		return _documentList.size();
	}

	@Override
	public Object getSelectedItem() 
	{
		return _selection;
	}

	@Override
	public void setSelectedItem(Object obj) 
	{
		_selection = (Document) obj;
	}
}