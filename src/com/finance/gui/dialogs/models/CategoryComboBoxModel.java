package com.finance.gui.dialogs.models;

import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import com.finance.model.Category;

public class CategoryComboBoxModel extends AbstractListModel implements ComboBoxModel 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 35683199336297212L;

	private List<Category> _categoryList;
	
	private Category _selection = null;
	
	public CategoryComboBoxModel(List<Category> categoryList) 
	{
		_categoryList = categoryList;
	}
	
	@Override
	public Object getElementAt(int index) 
	{
		return _categoryList.get(index);
	}

	@Override
	public int getSize() 
	{
		return _categoryList.size();
	}

	@Override
	public Object getSelectedItem() 
	{
		return _selection;
	}

	@Override
	public void setSelectedItem(Object obj) 
	{
		_selection = (Category) obj;
	}
}
