package com.finance.gui.dialogs.models;

import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import com.finance.model.Account;

public class AccountComboBoxModel extends AbstractListModel implements ComboBoxModel
{
	private List<Account> _accountList;
	
	private Account _selection = null;
	
	public AccountComboBoxModel(List<Account> accountList) 
	{
		_accountList = accountList;
	}
	
	@Override
	public Object getElementAt(int index) 
	{
		return _accountList.get(index);
	}

	@Override
	public int getSize() 
	{
		return _accountList.size();
	}

	@Override
	public Object getSelectedItem() 
	{
		return _selection;
	}

	@Override
	public void setSelectedItem(Object obj) 
	{
		_selection = (Account) obj;
	}
}