package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class TaxDialog 
{
	private JFrame _parent;
	private JDialog _dialog;
	private double[] _taxes;
	
	private JTextField _smallTax;
	private JTextField _largeTax;
	
	
	public TaxDialog (JFrame parent, double[] taxes)
    {
		_parent = parent;
		_taxes = taxes;
    }
	
	
    private JPanel initPanel()
    {
    	JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		JPanel buttonPanel = new JPanel(new GridLayout());
		
		fieldsPanel.add(new JLabel("������ �����"));
		_smallTax = new JTextField(5);
		_smallTax.setText(Double.toString(_taxes[0]));
		fieldsPanel.add(_smallTax,"wrap");
		
		fieldsPanel.add(new JLabel("������� �����"));
		_largeTax = new JTextField(5);
		_largeTax.setText(Double.toString(_taxes[1]));
		fieldsPanel.add(_largeTax, "wrap");

        JButton confirmButton = new JButton("OK");

        confirmButton.addActionListener(new ActionListener()
        {
            @Override 
            public void actionPerformed(ActionEvent a)
            {
            	if(checkFields())
            	{
            		_taxes[0] = Double.parseDouble(_smallTax.getText().toString());
            		_taxes[1] = Double.parseDouble(_largeTax.getText().toString());
            		_dialog.dispose();	
            	}
                
            }
        });
        
        JButton cancelButton = new JButton("�����");
        cancelButton.addActionListener (new ActionListener ()
        {
            @Override 
            public void actionPerformed(ActionEvent a)
            {
            	_dialog.dispose();
            }
        });
        
        buttonPanel.add(confirmButton);
        buttonPanel.add(cancelButton);


		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
        return dialogPanel;
    }

    public void display()
    {
        final int DWIDTH = 240;
        final int DHEIGHT = 120;

        _dialog = new JDialog (_parent, "����������� �.�.�.", true);
        _dialog.setSize(DWIDTH, DHEIGHT);
        _dialog.setResizable(false);
        _dialog.setDefaultCloseOperation (JDialog.DISPOSE_ON_CLOSE);

        _dialog.setContentPane(initPanel());

        _dialog.setLocationRelativeTo(_parent);
        _dialog.setVisible(true);
    }

    public double[] getTaxes()
    {
        return _taxes;
    }
    
    private boolean checkFields()
    {
    	String smallTax = _smallTax.getText().toString();
    	String largeTax = _largeTax.getText().toString();
    	
    	
    	if(smallTax.isEmpty() || largeTax.isEmpty())
    	{
			JOptionPane.showMessageDialog(_parent, "�������� ����������� �.�.�.",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
    	}
    	
    	if(!Utilities.isDouble(smallTax) || !Utilities.isDouble(largeTax))
		{
			JOptionPane.showMessageDialog(_parent, "�� ����������� �.�.�. ������ �� ����� �������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}

    	double smallTaxValue = Double.parseDouble(smallTax); 
    	double largeTaxValue = Double.parseDouble(largeTax);
		if((smallTaxValue < 0.0 || smallTaxValue > 100) || (largeTaxValue < 0.0 || largeTaxValue > 100))
		{
			JOptionPane.showMessageDialog(_parent, "�� ����������� �.�.�. ������ �� ����� ����� ��� ������ 0-100",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
    }
	
}
