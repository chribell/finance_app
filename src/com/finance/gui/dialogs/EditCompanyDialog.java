package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.controller.CompanyController;
import com.finance.model.Company;
import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class EditCompanyDialog extends JFrame 
{
	private Company _company;
	
	private JTextField _name;
	private JTextField _registryNumber; 
	private JTextField _expertise; 
	private JTextField _city; 
	private JTextField _address; 
	private JTextField _fax;
	private JTextField _title; 
	private JTextField _taxStore; 
	private JTextField _email; 
	private JTextField _zipCode; 
	private JTextField _phone;
	private JTextField _startDate;
	
	private JButton _confirmButton;
	private JButton _cancelButton;
	
	private CompanyController _controller;
	
	public EditCompanyDialog()
	{
		super("����������� ��������");
		_controller = new CompanyController();
		_company = _controller.getLastCompany();
		if(_company != null)
		{
			initGUI();
		}
		else
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "��� ������� �������",  "������", JOptionPane.ERROR_MESSAGE);
			dispatchEvent(new WindowEvent(EditCompanyDialog.this, WindowEvent.WINDOW_CLOSING));
		}		
	}
	
	private void initGUI()
	{
		this.setLocationRelativeTo(null);
		this.setSize(500, 300);
		JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		
		fieldsPanel.add(new JLabel("�����"));
		_name = new JTextField(_company.get_name(), 20);
		fieldsPanel.add(_name);
		
		fieldsPanel.add(new JLabel("������"));
		_title = new JTextField(_company.get_title(), 20);
		fieldsPanel.add(_title, "wrap");
		
		fieldsPanel.add(new JLabel("�.�.�."));
		_registryNumber = new JTextField(_company.get_registryNumber(), 20);
		fieldsPanel.add(_registryNumber);
		
		fieldsPanel.add(new JLabel("�.�.�."));
		_taxStore = new JTextField(_company.get_taxStore(), 20);
		fieldsPanel.add(_taxStore, "wrap");
		
		fieldsPanel.add(new JLabel("����������� ��������"));
		_expertise = new JTextField(_company.get_expertise(), 20);
		fieldsPanel.add(_expertise);
		
		fieldsPanel.add(new JLabel("E-mail"));
		_email = new JTextField(_company.get_email(), 20);
		fieldsPanel.add(_email, "wrap");		
		
		fieldsPanel.add(new JLabel("����"));
		_city = new JTextField(_company.get_city(), 20);
		fieldsPanel.add(_city);
		
		fieldsPanel.add(new JLabel("������������ �������"));
		_zipCode = new JTextField(_company.get_zipCode(), 20);
		fieldsPanel.add(_zipCode, "wrap");
		
		fieldsPanel.add(new JLabel("���������"));
		_address = new JTextField(_company.get_address(), 20);
		fieldsPanel.add(_address);
		
		fieldsPanel.add(new JLabel("��������"));
		_phone = new JTextField(_company.get_phone(), 20);
		fieldsPanel.add(_phone, "wrap");
		
		fieldsPanel.add(new JLabel("Fax"));
		_fax = new JTextField(_company.get_fax(), 20);
		fieldsPanel.add(_fax);
		
		fieldsPanel.add(new JLabel("��/��� �������"));
		_startDate = new JTextField(_company.get_startDate(), 20);
		fieldsPanel.add(_startDate);
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(checkFields())
				{
					_company = new Company(_company.get_id(),
											 _name.getText().toString(),
											 _registryNumber.getText().toString(),
											 _expertise.getText().toString(),
											 _city.getText().toString(),
											 _address.getText().toString(),
											 _fax.getText().toString(),
											 _title.getText().toString(),
										 	 _taxStore.getText().toString(),
										 	 _email.getText().toString(),
									 	 	 _zipCode.getText().toString(), 
									 	 	 _phone.getText().toString(),
									 	 	 _startDate.getText().toString()	 
											);
					if(_controller.updateCompany(_company))
						JOptionPane.showMessageDialog(EditCompanyDialog.this, "�������� ����������� ��������",  "���������", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(EditCompanyDialog.this, "���������� ����������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(EditCompanyDialog.this, WindowEvent.WINDOW_CLOSING));
				}
				
			}
			
		});
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				dispatchEvent(new WindowEvent(EditCompanyDialog.this, WindowEvent.WINDOW_CLOSING));
			}
			
		});
		
		buttonPanel.add(_confirmButton);
		buttonPanel.add(_cancelButton);
		
		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(dialogPanel);
	}
	
	
	private boolean checkFields()
	{
		String name = _name.getText().toString();
		if(name.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		String registryNumber = _registryNumber.getText().toString();
		if(registryNumber.length() != 9)
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "�� ��� ������ �� ����� 9�����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if(!Utilities.isInteger(registryNumber))
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "�� ��� ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		String expertise = _expertise.getText().toString();
		if(expertise.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� ����������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String city = _city.getText().toString();
		if(city.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� ����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String address = _address.getText().toString();
		if(address.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� ���������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String fax = _fax.getText().toString();
		if(fax.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� fax",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isInteger(fax))
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "�� fax ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String title = _title.getText().toString();
		if(title.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String taxStore = _taxStore.getText().toString();
		if(taxStore.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� �.�.�.",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String email = _email.getText().toString();
		if(email.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� email",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String zipCode = _zipCode.getText().toString();
		if(zipCode.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� ����������� ������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isInteger(zipCode))
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "� ������������ ������� ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String phone = _phone.getText().toString();
		if(phone.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isInteger(phone))
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "�� �������� ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String startDate = _startDate.getText().toString();
		if(startDate.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "������ �� �������� ���������� �������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isDate(startDate))
		{
			JOptionPane.showMessageDialog(EditCompanyDialog.this, "� ���������� ������ �� ����� ��� ������ dd/mm/yyyy",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
}
