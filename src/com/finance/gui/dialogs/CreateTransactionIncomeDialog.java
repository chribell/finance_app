package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.controller.CategoryIncomeController;
import com.finance.controller.ClientController;
import com.finance.controller.DocumentIncomeController;
import com.finance.controller.IncomeTransactionController;
import com.finance.gui.dialogs.models.AccountComboBoxModel;
import com.finance.gui.dialogs.models.CategoryComboBoxModel;
import com.finance.gui.dialogs.models.DocumentComboBoxModel;
import com.finance.gui.dialogs.models.TransactionTypeComboBoxModel;
import com.finance.model.Account;
import com.finance.model.Category;
import com.finance.model.Document;
import com.finance.model.Transaction;
import com.finance.model.TransactionType;
import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class CreateTransactionIncomeDialog extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4841861017813823370L;

	
	private double[] _taxes; 
	private JComboBox<TransactionType> _transactionComboBox;
	private JComboBox<Document> _documentComboBox;
	private JComboBox<Category> _categoryComboBox;
	private JComboBox<Account> _clientComboBox;
	private JTextField _date;
	private JTextField _amount;
	
	private JButton _confirmButton;
	private JButton _cancelButton;
	
	private IncomeTransactionController _incomeController;
	private CategoryIncomeController _categoryController;
	private DocumentIncomeController _documentController;
	private ClientController _clientController;
	
	
	private List<Category> _categories;
	private List<Document> _documents;
	private List<Account> _clients;
	
	private boolean _isWholesale;
	
	public CreateTransactionIncomeDialog(double[] taxes) 
	{
		super("�����");
		_taxes = taxes;
		_incomeController = new IncomeTransactionController();
		_categoryController = new CategoryIncomeController();
		_documentController = new DocumentIncomeController();
		_clientController = new ClientController();
		_categories = _categoryController.getAll();
		_documents = _documentController.getAll();
		_clients = _clientController.getAll();
		initGUI();
	}

	
	public void initGUI() 
	{
		this.setLocationRelativeTo(null);
		this.setSize(400, 300);
		JPanel masterPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		
		_isWholesale = false;
		
		
		fieldsPanel.add(new JLabel("�/�"));
		JTextField code = new JTextField(5);
		code.setText((_incomeController.getNumberOfIncomeTransactions() + 1) + "");
		code.setEnabled(false);
		fieldsPanel.add(code, "wrap");
		
		fieldsPanel.add(new JLabel("����������"));
		_date = new JTextField(9);
		fieldsPanel.add(_date, "wrap");
		
		fieldsPanel.add(new JLabel("�����������"));
		_documentComboBox = new JComboBox<Document>(new DocumentComboBoxModel(_documents));
		fieldsPanel.add(_documentComboBox, "wrap");
		
		fieldsPanel.add(new JLabel("����������"));
		_categoryComboBox = new JComboBox<Category>(new CategoryComboBoxModel(_categories));
		fieldsPanel.add(_categoryComboBox, "wrap");
		
		List<TransactionType> typeList = new ArrayList<>();
		typeList.add(new TransactionType(1, "��������� ��������", _taxes[0]));
		typeList.add(new TransactionType(2, "��������� ��������", _taxes[1]));
		typeList.add(new TransactionType(3, "�������� ��������", _taxes[0]));
		typeList.add(new TransactionType(4, "�������� ��������", _taxes[1]));
		typeList.add(new TransactionType(5, "�������� ������", _taxes[1]));
		typeList.add(new TransactionType(6, "����� �����", _taxes[1]));
		_transactionComboBox = new JComboBox<TransactionType>(new TransactionTypeComboBoxModel(typeList));
		
		_transactionComboBox.addItemListener(new ItemListener() 
		{
			public void itemStateChanged( ItemEvent event ) 
			{
				if (event.getStateChange() == ItemEvent.SELECTED) 
				{
					TransactionType selectedType = (TransactionType) event.getItem();
					if(selectedType.get_id() == 1 || selectedType.get_id() == 2) //selected type is wholesale
					{
						_isWholesale = true;
						_clientComboBox.setEnabled(true);
					}
					else
					{
						_isWholesale = false;
						_clientComboBox.setEnabled(false);
					}
		        }
			}
			
		});
		
		fieldsPanel.add(new JLabel("�����"));
		fieldsPanel.add(_transactionComboBox, "wrap");
		
		_clientComboBox = new JComboBox<Account>(new AccountComboBoxModel(_clients));
		_clientComboBox.setEnabled(false);
		fieldsPanel.add(new JLabel("�������"));
		fieldsPanel.add(_clientComboBox, "wrap");
		
		fieldsPanel.add(new JLabel("����"));
		_amount = new JTextField(9);
		fieldsPanel.add(_amount);
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(checkFields())
				{
					Transaction transaction = new Transaction(_incomeController.getNumberOfIncomeTransactions() + 1,
																((TransactionType) _transactionComboBox.getSelectedItem()).get_id(),
																_date.getText().toString(),
																((Category) _categoryComboBox.getSelectedItem()),
																((Document) _documentComboBox.getSelectedItem()),
																Double.parseDouble(_amount.getText().toString()));
					if(_isWholesale)
					{
						transaction.setAccount((Account) _clientComboBox.getSelectedItem());
					}
							
					if(_incomeController.createTransactionIncome(transaction))
						JOptionPane.showMessageDialog(CreateTransactionIncomeDialog.this, "�������� ���������� ������",  "���������", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(CreateTransactionIncomeDialog.this, "���������� ���������� ������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(CreateTransactionIncomeDialog.this, WindowEvent.WINDOW_CLOSING));
				}
			}
			
		});
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				dispatchEvent(new WindowEvent(CreateTransactionIncomeDialog.this, WindowEvent.WINDOW_CLOSING));
			}
			
		});
		
		buttonPanel.add(_confirmButton);
		buttonPanel.add(_cancelButton);
		
		masterPanel.add(fieldsPanel, BorderLayout.CENTER);
		masterPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(masterPanel);
	}

	private boolean checkFields()
	{
		String date = _date.getText().toString();
		if(date.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateTransactionIncomeDialog.this, "������ �� �������� ����������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isDate(date))
		{
			JOptionPane.showMessageDialog(CreateTransactionIncomeDialog.this, "� ���������� ������ �� ����� ��� ������ dd/mm/yyyy",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		Document document = (Document) _documentComboBox.getSelectedItem();
		if(document == null)
		{
			JOptionPane.showMessageDialog(CreateTransactionIncomeDialog.this, "������ �� ��������� �����������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		Category category = (Category) _categoryComboBox.getSelectedItem();
		if(category == null)
		{
			JOptionPane.showMessageDialog(CreateTransactionIncomeDialog.this, "������ �� ��������� ����������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		TransactionType type = (TransactionType) _transactionComboBox.getSelectedItem();
		if(type == null)
		{
			JOptionPane.showMessageDialog(CreateTransactionIncomeDialog.this, "������ �� ��������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(_isWholesale)
		{
			Account client = (Account) _clientComboBox.getSelectedItem();
			if(client == null)
			{
				JOptionPane.showMessageDialog(CreateTransactionIncomeDialog.this, "������ �� ��������� ������",  "������", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		
		String amount = _amount.getText().toString();
		if(!Utilities.isDouble(amount))
		{
			JOptionPane.showMessageDialog(CreateTransactionIncomeDialog.this, "�������� ����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
	
}
