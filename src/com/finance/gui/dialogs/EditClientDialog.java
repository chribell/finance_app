package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.controller.ClientController;
import com.finance.model.Account;
import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class EditClientDialog extends JFrame
{
	private Account _account;
	
	private JTextField _searchCode;
	private JTextField _code;
	private JTextField _name;
	private JTextField _inCharge;
	private JTextField _occupation;
	private JTextField _city;
	private JTextField _zipCode;
	private JTextField _registryNumber;
	private JTextField _taxStore;
	private JTextField _address;
	private JTextField _phone;
	
	private JButton _searchButton;
	private JButton _confirmButton;
	private JButton _deleteButton;
	private JButton _cancelButton;
	
	private ClientController _controller;
	
	public EditClientDialog()
	{
		super("����������� ������");
		_controller = new ClientController();
		initGUI();
	}
	
	private void initGUI() 
	{
		this.setLocationRelativeTo(null);
		this.setSize(500, 300);
		JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		JPanel searchPanel = new JPanel(new FlowLayout());
		
		searchPanel.add(new JLabel("�������"));
		_searchCode = new JTextField(20);
		searchPanel.add(_searchCode);
		
		_searchButton = new JButton("���������");
		_searchButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String searchCode = _searchCode.getText().toString();
				//System.out.println("Search code: " + searchCode);
				if(!Utilities.isClientCode(searchCode))
				{
					JOptionPane.showMessageDialog(EditClientDialog.this, "������ �� �������� ������ ��� ������ 30.��.��.��",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				_account = _controller.getClientByCode(searchCode);
				if(_account != null)
				{
					enableFields();
					setFieldsValues();
				}
				else
				{
					JOptionPane.showMessageDialog(EditClientDialog.this, "��� ������� ������������� �������",  "������", JOptionPane.ERROR_MESSAGE);
					disableFields();
				}
			}
		});
		searchPanel.add(_searchButton);
		
		fieldsPanel.add(new JLabel("�������"));
		_code = new JTextField(20);
		
		fieldsPanel.add(_code);
		
		fieldsPanel.add(new JLabel("������������� / �������"));
		_name = new JTextField(20);
		fieldsPanel.add(_name, "wrap");
		
		fieldsPanel.add(new JLabel("���������"));
		_inCharge = new JTextField(20);
		fieldsPanel.add(_inCharge);
		
		fieldsPanel.add(new JLabel("���������"));
		_occupation = new JTextField(20);
		fieldsPanel.add(_occupation, "wrap");
		
		fieldsPanel.add(new JLabel("����"));
		_city = new JTextField(20);
		fieldsPanel.add(_city);
		
		fieldsPanel.add(new JLabel("������������ �������"));
		_zipCode = new JTextField(20);
		fieldsPanel.add(_zipCode, "wrap");		
		
		fieldsPanel.add(new JLabel("�.�.�."));
		_registryNumber = new JTextField(20);
		fieldsPanel.add(_registryNumber);
		
		fieldsPanel.add(new JLabel("�.�.�."));
		_taxStore = new JTextField(20);
		fieldsPanel.add(_taxStore, "wrap");
		
		fieldsPanel.add(new JLabel("���������"));
		_address = new JTextField(20);
		fieldsPanel.add(_address);
		
		fieldsPanel.add(new JLabel("��������"));
		_phone = new JTextField(20);
		fieldsPanel.add(_phone, "wrap");
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(checkFields())
				{
					_account = new Account(_account.get_id(),
										   _code.getText().toString(),
										   _name.getText().toString(),
										   _inCharge.getText().toString(),
										   _occupation.getText().toString(),
										   _city.getText().toString(),	
										   _zipCode.getText().toString(),
										   _registryNumber.getText().toString(),
										   _taxStore.getText().toString(),
										   _address.getText().toString(),
										   _phone.getText().toString()  
										   );
					if(_controller.updateClient(_account))
						JOptionPane.showMessageDialog(EditClientDialog.this, "�������� ������ ������",  "���������", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(EditClientDialog.this, "���������� ������ ������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(EditClientDialog.this, WindowEvent.WINDOW_CLOSING));
				}
				
			}
			
		});
		
		_deleteButton = new JButton("��������");
		_deleteButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				Object[] options = { "OK", "�����" };
				int choice = JOptionPane.showOptionDialog(null, "������� OK ��� �� �������� ��� ������", "�������",
						JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
						null, options, options[0]);
				if(choice == 0)
				{
					if(_controller.deleteteClient(_account.get_id()))
					
						JOptionPane.showMessageDialog(EditClientDialog.this, "�������� �������� ������",  "���������", JOptionPane.INFORMATION_MESSAGE);						
					else
						JOptionPane.showMessageDialog(EditClientDialog.this, "���������� �������� ������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(EditClientDialog.this, WindowEvent.WINDOW_CLOSING));
				}
			}
			
		});
		
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				dispatchEvent(new WindowEvent(EditClientDialog.this, WindowEvent.WINDOW_CLOSING));
				
			}
			
		});
		
		buttonPanel.add(_confirmButton);
		buttonPanel.add(_deleteButton);
		buttonPanel.add(_cancelButton);
		
		dialogPanel.add(searchPanel, BorderLayout.NORTH);
		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(dialogPanel);
		disableFields();
		
	}

	private boolean checkFields()
	{
		String name = _name.getText().toString();
		if(name.isEmpty())
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "������ �� �������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String inCharge = _inCharge.getText().toString();
		if(inCharge.isEmpty())
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String occupation = _occupation.getText().toString();
		if(occupation.isEmpty())
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "������ �� �������� ���������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String city = _city.getText().toString();
		if(city.isEmpty())
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "������ �� �������� ����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String zipCode = _zipCode.getText().toString();
		if(zipCode.isEmpty())
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "������ �� �������� ����������� ������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isInteger(zipCode))
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "� ������������ ������� ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String registryNumber = _registryNumber.getText().toString();
		if(registryNumber.length() != 9)
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "�� ��� ������ �� ����� 9�����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if(!Utilities.isInteger(registryNumber))
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "�� ��� ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String taxStore = _taxStore.getText().toString();
		if(taxStore.isEmpty())
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "������ �� �������� �.�.�.",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String address = _address.getText().toString();
		if(address.isEmpty())
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "������ �� �������� ���������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		

		String phone = _phone.getText().toString();
		if(phone.isEmpty())
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isInteger(phone))
		{
			JOptionPane.showMessageDialog(EditClientDialog.this, "�� �������� ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
	
	private void disableFields()
	{
		_code.setEnabled(false);
		_name.setEnabled(false);
		_inCharge.setEnabled(false);
		_occupation.setEnabled(false);
		_city.setEnabled(false);
		_zipCode.setEnabled(false);
		_registryNumber.setEnabled(false);
		_taxStore.setEnabled(false);
		_address.setEnabled(false);
		_phone.setEnabled(false);
		
		_confirmButton.setEnabled(false);
		_deleteButton.setEnabled(false);
	}
	
	private void enableFields()
	{
		_name.setEnabled(true);
		_inCharge.setEnabled(true);
		_occupation.setEnabled(true);
		_city.setEnabled(true);
		_zipCode.setEnabled(true);
		_registryNumber.setEnabled(true);
		_taxStore.setEnabled(true);
		_address.setEnabled(true);
		_phone.setEnabled(true);
		
		_confirmButton.setEnabled(true);
		_deleteButton.setEnabled(true);
	}
	
	private void setFieldsValues()
	{
		_code.setText(_account.get_code());
		_name.setText(_account.get_name());
		_inCharge.setText(_account.get_inCharge());
		_occupation.setText(_account.get_occupation());
		_city.setText(_account.get_city());
		_zipCode.setText(_account.get_zipCode());
		_registryNumber.setText(_account.get_registryNumber());
		_taxStore.setText(_account.get_taxStore());
		_address.setText(_account.get_address());
		_phone.setText(_account.get_phone());
	}
}
