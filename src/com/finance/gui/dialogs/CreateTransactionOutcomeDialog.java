package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

import com.finance.controller.CategoryOutcomeController;
import com.finance.controller.SupplierController;
import com.finance.controller.DocumentOutcomeController;
import com.finance.controller.OutcomeTranstactionController;
import com.finance.gui.dialogs.models.AccountComboBoxModel;
import com.finance.gui.dialogs.models.CategoryComboBoxModel;
import com.finance.gui.dialogs.models.DocumentComboBoxModel;
import com.finance.gui.dialogs.models.TransactionTypeComboBoxModel;
import com.finance.model.Account;
import com.finance.model.Category;
import com.finance.model.Document;
import com.finance.model.Transaction;
import com.finance.model.TransactionType;
import com.finance.util.Utilities;

public class CreateTransactionOutcomeDialog extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8131748170408310728L;
	
	private double[] _taxes; 
	private JComboBox<TransactionType> _transactionComboBox;
	private JComboBox<Document> _documentComboBox;
	private JComboBox<Category> _categoryComboBox;
	private JComboBox<Account> _supplierComboBox;
	private JTextField _date;
	private JTextField _amount;
	
	private JButton _confirmButton;
	private JButton _cancelButton;
	
	private OutcomeTranstactionController _outcomeController;
	private CategoryOutcomeController _categoryController;
	private DocumentOutcomeController _documentController;
	private SupplierController _clientController;
	
	private List<Category> _categories;
	private List<Document> _documents;
	private List<Account> _suppliers;
	
	private boolean _isPurchase;

	public CreateTransactionOutcomeDialog(double[] taxes) 
	{
		super("�����");
		_taxes = taxes;
		_outcomeController = new OutcomeTranstactionController();
		_categoryController = new CategoryOutcomeController();
		_documentController = new DocumentOutcomeController();
		_clientController = new SupplierController();
		_categories = _categoryController.getAll();
		_documents = _documentController.getAll();
		_suppliers = _clientController.getAll();
		initGUI();
	}
	
	public void initGUI() 
	{
		this.setLocationRelativeTo(null);
		this.setSize(400, 300);
		JPanel masterPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		
		_isPurchase = false;
		
		fieldsPanel.add(new JLabel("�/�"));
		JTextField code = new JTextField(5);
		code.setText((_outcomeController.getNumberOfOutcomeTransactions() + 1) + "");
		code.setEnabled(false);
		fieldsPanel.add(code, "wrap");
		
		fieldsPanel.add(new JLabel("����������"));
		_date = new JTextField(9);
		fieldsPanel.add(_date, "wrap");
		
		fieldsPanel.add(new JLabel("�����������"));
		_documentComboBox = new JComboBox<Document>(new DocumentComboBoxModel(_documents));
		fieldsPanel.add(_documentComboBox, "wrap");
		
		fieldsPanel.add(new JLabel("����������"));
		_categoryComboBox = new JComboBox<Category>(new CategoryComboBoxModel(_categories));
		fieldsPanel.add(_categoryComboBox, "wrap");
		
		List<TransactionType> typeList = new ArrayList<>();
		typeList.add(new TransactionType(1, "������", _taxes[0]));
		typeList.add(new TransactionType(2, "������", _taxes[1]));
		typeList.add(new TransactionType(3, "������� �� �������� ��������", _taxes[1]));
		typeList.add(new TransactionType(4, "������� ����� �������� ��������", _taxes[2]));
		typeList.add(new TransactionType(5, "�����", _taxes[1]));
		typeList.add(new TransactionType(6, "����� �����", _taxes[1]));
		_transactionComboBox = new JComboBox<TransactionType>(new TransactionTypeComboBoxModel(typeList));
		
		_transactionComboBox.addItemListener(new ItemListener() 
		{
			public void itemStateChanged( ItemEvent event ) 
			{
				if (event.getStateChange() == ItemEvent.SELECTED) 
				{
					TransactionType selectedType = (TransactionType) event.getItem();
					if(selectedType.get_id() == 1 || selectedType.get_id() == 2) //selected type is purchase
					{
						_isPurchase = true;
						_supplierComboBox.setEnabled(true);
					}
					else
					{
						_isPurchase = false;
						_supplierComboBox.setEnabled(false);
					}
		        }
			}
			
		});
		
		fieldsPanel.add(new JLabel("�����"));
		fieldsPanel.add(_transactionComboBox, "wrap");
		
		_supplierComboBox = new JComboBox<Account>(new AccountComboBoxModel(_suppliers));
		_supplierComboBox.setEnabled(false);
		fieldsPanel.add(new JLabel("�����������"));
		fieldsPanel.add(_supplierComboBox, "wrap");
		
		fieldsPanel.add(new JLabel("����"));
		_amount = new JTextField(9);
		fieldsPanel.add(_amount);
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(checkFields())
				{
					Transaction transaction = new Transaction(_outcomeController.getNumberOfOutcomeTransactions() + 1,
																((TransactionType) _transactionComboBox.getSelectedItem()).get_id(),
																_date.getText().toString(),
																((Category) _categoryComboBox.getSelectedItem()),
																((Document) _documentComboBox.getSelectedItem()),
																Double.parseDouble(_amount.getText().toString()));
					
					if(_isPurchase)
					{
						transaction.setAccount((Account) _supplierComboBox.getSelectedItem());
					}
							
					if(_outcomeController.createTransactionOutcome(transaction))
						JOptionPane.showMessageDialog(CreateTransactionOutcomeDialog.this, "�������� ���������� ������",  "���������", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(CreateTransactionOutcomeDialog.this, "���������� ���������� ������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(CreateTransactionOutcomeDialog.this, WindowEvent.WINDOW_CLOSING));
				}
			}
			
		});
		
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				dispatchEvent(new WindowEvent(CreateTransactionOutcomeDialog.this, WindowEvent.WINDOW_CLOSING));
			}
			
		});
		
		buttonPanel.add(_confirmButton);
		buttonPanel.add(_cancelButton);
		
		masterPanel.add(fieldsPanel, BorderLayout.CENTER);
		masterPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(masterPanel);
	}
	
	private boolean checkFields()
	{
		String date = _date.getText().toString();
		if(date.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateTransactionOutcomeDialog.this, "������ �� �������� ����������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isDate(date))
		{
			JOptionPane.showMessageDialog(CreateTransactionOutcomeDialog.this, "� ���������� ������ �� ����� ��� ������ dd/mm/yyyy",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		Document document = (Document) _documentComboBox.getSelectedItem();
		if(document == null)
		{
			JOptionPane.showMessageDialog(CreateTransactionOutcomeDialog.this, "������ �� ��������� �����������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		Category category = (Category) _categoryComboBox.getSelectedItem();
		if(category == null)
		{
			JOptionPane.showMessageDialog(CreateTransactionOutcomeDialog.this, "������ �� ��������� ����������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		TransactionType type = (TransactionType) _transactionComboBox.getSelectedItem();
		if(type == null)
		{
			JOptionPane.showMessageDialog(CreateTransactionOutcomeDialog.this, "������ �� ��������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(_isPurchase)
		{
			Account supplier = (Account) _supplierComboBox.getSelectedItem();
			if(supplier == null)
			{
				JOptionPane.showMessageDialog(CreateTransactionOutcomeDialog.this, "������ �� ��������� ������",  "������", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		
		String amount = _amount.getText().toString();
		if(!Utilities.isDouble(amount))
		{
			JOptionPane.showMessageDialog(CreateTransactionOutcomeDialog.this, "�������� ����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
}
