package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

import com.finance.controller.OutcomeTranstactionController;
import com.finance.model.Transaction;
import com.finance.util.Utilities;

public class DeleteOutcomeTransaction extends JFrame
{
	private Transaction _transaction;
	
	private JTextField _searchID;
	private JTextField _id;
	private JTextField _date;
	private JTextField _document;
	private JTextField _category;
	private JTextField _type;
	private JTextField _supplier;
	private JTextField _amount;
	
	
	private JButton _searchButton;
	private JButton _deleteButton;
	private JButton _cancelButton;
	
	private OutcomeTranstactionController _controller;
	
	public DeleteOutcomeTransaction()
	{
		super("�������� ������� ������");
		_controller = new OutcomeTranstactionController();
		initGUI();
	}
	
	private void initGUI() 
	{
		this.setLocationRelativeTo(null);
		this.setSize(500, 300);
		JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		JPanel searchPanel = new JPanel(new FlowLayout());
		
		searchPanel.add(new JLabel("�������"));
		_searchID = new JTextField(20);
		searchPanel.add(_searchID);
		
		_searchButton = new JButton("���������");
		_searchButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String searchID = _searchID.getText().toString();
				if(!Utilities.isInteger(searchID))
				{
					JOptionPane.showMessageDialog(DeleteOutcomeTransaction.this, "������ �� �������� ���� ������� ������",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				_transaction = _controller.getTransactionByID(Integer.parseInt(searchID));
				if(_transaction != null)
				{
					setFieldsValues();
				}
				else
				{
					JOptionPane.showMessageDialog(DeleteOutcomeTransaction.this, "��� ������� ������������ ������ ������ �� ���� ��� ������",  "������", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		searchPanel.add(_searchButton);
		
		fieldsPanel.add(new JLabel("�������"));
		_id = new JTextField(20);
		fieldsPanel.add(_id, "wrap");
		
		fieldsPanel.add(new JLabel("����������"));
		_date = new JTextField(20);
		fieldsPanel.add(_date, "wrap");
		
		fieldsPanel.add(new JLabel("�����������"));
		_document = new JTextField(20);
		fieldsPanel.add(_document, "wrap");
		
		fieldsPanel.add(new JLabel("����������"));
		_category = new JTextField(20);
		fieldsPanel.add(_category, "wrap");
		
		fieldsPanel.add(new JLabel("�����"));
		_type = new JTextField(20);
		fieldsPanel.add(_type, "wrap");
		
		fieldsPanel.add(new JLabel("�����������"));
		_supplier = new JTextField(20);
		fieldsPanel.add(_supplier, "wrap");		
		
		fieldsPanel.add(new JLabel("����"));
		_amount = new JTextField(20);
		fieldsPanel.add(_amount, "wrap");
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		
		_deleteButton = new JButton("��������");
		_deleteButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(_transaction == null) return;
				Object[] options = { "OK", "�����" };
				int choice = JOptionPane.showOptionDialog(null, "������� OK ��� �� �������� ��� ����������", "�������",
						JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
						null, options, options[0]);
				if(choice == 0)
				{
					if(_controller.deleteTransactionOutcome(_transaction.getID()))
					
						JOptionPane.showMessageDialog(DeleteOutcomeTransaction.this, "�������� �������� ����������",  "���������", JOptionPane.INFORMATION_MESSAGE);						
					else
						JOptionPane.showMessageDialog(DeleteOutcomeTransaction.this, "���������� �������� ����������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(DeleteOutcomeTransaction.this, WindowEvent.WINDOW_CLOSING));
				}
			}
			
		});
		
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				dispatchEvent(new WindowEvent(DeleteOutcomeTransaction.this, WindowEvent.WINDOW_CLOSING));
				
			}
			
		});
		
		buttonPanel.add(_deleteButton);
		buttonPanel.add(_cancelButton);
		
		dialogPanel.add(searchPanel, BorderLayout.NORTH);
		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(dialogPanel);
		disableFields();
		
	}

	private void disableFields()
	{
		_id.setEnabled(false);
		_date.setEnabled(false);
		_document.setEnabled(false);
		_category.setEnabled(false);
		_type.setEnabled(false);
		_supplier.setEnabled(false);
		_amount.setEnabled(false);
	}
	
	private void setFieldsValues()
	{
		_id.setText(_transaction.getID() + "");
		_date.setText(_transaction.getDate());
		_document.setText(_transaction.getDocument().get_abbreviation());
		_category.setText(_transaction.getCategory().get_explanation());
		_type.setText(_transaction.getTypeID() + "");
		_supplier.setText(_transaction.getAccount() == null ? "" : _transaction.getAccount().get_code());
		_amount.setText(_transaction.getAmount() + "");
	}
}
