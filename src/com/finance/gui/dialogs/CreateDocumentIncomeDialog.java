package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.controller.DocumentIncomeController;
import com.finance.model.Document;
import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class CreateDocumentIncomeDialog extends JFrame
{

	private JTextField _name;
	private JTextField _abbreviation; 
	private JCheckBox _requirementRegistryNumber;
	
	
	private JButton _confirmButton;
	private JButton _cancelButton;
	
	private DocumentIncomeController _controller;
	
	public CreateDocumentIncomeDialog()
	{
		super("�������� ������������ ������");
		_controller = new DocumentIncomeController();
		initGUI();
	}
	
	private void initGUI()
	{
		this.setLocationRelativeTo(null);
		this.setSize(400, 200);
		JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		
		fieldsPanel.add(new JLabel("�����"));
		_name = new JTextField(20);
		fieldsPanel.add(_name, "wrap");
		
		fieldsPanel.add(new JLabel("�������������"));
		_abbreviation = new JTextField(20);
		fieldsPanel.add(_abbreviation, "wrap");
		
		fieldsPanel.add(new JLabel("�������� �.�.�."));
		_requirementRegistryNumber = new JCheckBox();
		fieldsPanel.add(_requirementRegistryNumber);
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				int rRNNum = _requirementRegistryNumber.isSelected() ? 1 : 0;
				if(checkFields())
				{
					Document document = new Document(_controller.getNumberOfIncomeDocuments() + 1,
													 _name.getText().toString(),
													 _abbreviation.getText().toString(),
													 rRNNum											 	 
												);
					if(_controller.createDocumentIncome(document))
						JOptionPane.showMessageDialog(CreateDocumentIncomeDialog.this, "�������� ���������� ����������",  "���������", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(CreateDocumentIncomeDialog.this, "���������� ���������� ����������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(CreateDocumentIncomeDialog.this, WindowEvent.WINDOW_CLOSING));
				}
				
			}
			
		});
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				dispatchEvent(new WindowEvent(CreateDocumentIncomeDialog.this, WindowEvent.WINDOW_CLOSING));
				
			}
			
		});
		
		buttonPanel.add(_confirmButton);
		buttonPanel.add(_cancelButton);
		
		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(dialogPanel);
	}
	
	private boolean checkFields()
	{
		String name = _name.getText().toString();
		if(name.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateDocumentIncomeDialog.this, "������ �� �������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String abbreviation = _abbreviation.getText().toString();
		if(abbreviation.isEmpty())
		{
			JOptionPane.showMessageDialog(CreateDocumentIncomeDialog.this, "������ �� �������� �������������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
}
