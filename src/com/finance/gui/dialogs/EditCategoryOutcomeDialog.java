package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.controller.CategoryOutcomeController;
import com.finance.model.Category;
import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class EditCategoryOutcomeDialog extends JFrame
{
	private Category _category;
	
	private JTextField _searchCode;
	private JTextField _code;
	private JTextField _explanation; 
	private JTextField _field;
	
	private JButton _searchButton;
	private JButton _confirmButton;
	private JButton _cancelButton;
	
	private CategoryOutcomeController _controller;
	
	public EditCategoryOutcomeDialog()
	{
		super("����������� ����������� ������");
		_controller = new CategoryOutcomeController();
		initGUI();
	}
	
	private void initGUI()
	{
		this.setLocationRelativeTo(null);
		this.setSize(400, 200);
		JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		JPanel searchPanel = new JPanel(new FlowLayout());
		
		searchPanel.add(new JLabel("�������"));
		_searchCode = new JTextField(20);
		searchPanel.add(_searchCode);
		
		_searchButton = new JButton("���������");
		_searchButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String searchCode = _searchCode.getText().toString();
				if(searchCode.isEmpty())
				{
					JOptionPane.showMessageDialog(EditCategoryOutcomeDialog.this, "��� ������� ���������� ������ �� ����� ��� ������",  "������", JOptionPane.ERROR_MESSAGE);
					return;
				}
				_category = _controller.getCategoryOutcomeByCode(searchCode);
				if(_category != null)
				{
					enableFields();
					setFieldsValues();
				}
				else
				{
					JOptionPane.showMessageDialog(EditCategoryOutcomeDialog.this, "������ �� �������� ������ ��� ������ ��.��.��.��",  "������", JOptionPane.ERROR_MESSAGE);
					disableFields();
				}
			}
		});
		searchPanel.add(_searchButton);
		
		fieldsPanel.add(new JLabel("�������"));
		_code = new JTextField(20);
		fieldsPanel.add(_code, "wrap");
		
		fieldsPanel.add(new JLabel("���������"));
		_explanation = new JTextField(20);
		fieldsPanel.add(_explanation, "wrap");
		
		fieldsPanel.add(new JLabel("�����"));
		_field = new JTextField(20);
		fieldsPanel.add(_field);
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(checkFields())
				{
					int fieldNum = Integer.parseInt(_field.getText().toString());
					_category = new Category(_category.get_id(),
							  				 _code.getText().toString(),
							  				 _explanation.getText().toString(),
							  				 fieldNum											 	 
											);
					if(_controller.updateCategoryOutcome(_category))
						JOptionPane.showMessageDialog(EditCategoryOutcomeDialog.this, "�������� ������ ����������� ������",  "���������", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(EditCategoryOutcomeDialog.this, "���������� ������ ����������� ������",  "������", JOptionPane.ERROR_MESSAGE);
					dispatchEvent(new WindowEvent(EditCategoryOutcomeDialog.this, WindowEvent.WINDOW_CLOSING));
				}
			}
			
		});
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				dispatchEvent(new WindowEvent(EditCategoryOutcomeDialog.this, WindowEvent.WINDOW_CLOSING));
				
			}
			
		});
		
		buttonPanel.add(_confirmButton);
		buttonPanel.add(_cancelButton);
		
		dialogPanel.add(searchPanel, BorderLayout.NORTH);
		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
		this.getContentPane().add(dialogPanel);
		disableFields();
	}
	
	private boolean checkFields()
	{
		String code = _code.getText().toString();
		if(!Utilities.isCode(code))
		{
			JOptionPane.showMessageDialog(EditCategoryOutcomeDialog.this, "������ �� �������� ������ ��� ������ ��.��.��.��",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String explanation = _explanation.getText().toString();
		if(explanation.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCategoryOutcomeDialog.this, "������ �� �������� ���������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String field = _field.getText().toString();
		if(field.isEmpty())
		{
			JOptionPane.showMessageDialog(EditCategoryOutcomeDialog.this, "������ �� �������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isInteger(field))
		{
			JOptionPane.showMessageDialog(EditCategoryOutcomeDialog.this, "�� ����� ������ �� ��������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	private void disableFields()
	{
		_code.setEnabled(false);
		_explanation.setEnabled(false);
		_field.setEnabled(false);

		_confirmButton.setEnabled(false);
	}
	
	private void enableFields()
	{
		_explanation.setEnabled(true);
		_field.setEnabled(true);

		_confirmButton.setEnabled(true);
	}
	
	private void setFieldsValues()
	{
		_explanation.setText((_category.get_explanation()));
		_field.setText(_category.get_numberOfFields() + "");
	}
}
