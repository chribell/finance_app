package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.controller.IncomeTransactionController;
import com.finance.controller.OutcomeTranstactionController;
import com.finance.core.TransactionCalculator;
import com.finance.model.Transaction;
import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class VATForYieldDialog extends JFrame 
{
	
	private Transaction _transaction;
	
	private JComboBox _periods;
	private String[] _arrayPeriods;
	private JTextField _year;
	private JButton _select;
	private JButton _execute;
	private JButton _clear;
	
	private JTextField _inputsMin;
	private JTextField _inputsMax;
	private JTextField _sumOfInputs;
	private JTextField _inputTaxMin;
	private JTextField _inputTaxMax;
	private JTextField _sumOfInputTax;
	
	private JTextField _outputsMin;
	private JTextField _outputsMax;
	private JTextField _outputWithoutTax;
	private JTextField _sumOfOutputs;
	private JTextField _outputTaxMin;
	private JTextField _outputTaxMax;
	private JTextField _sumOfOutputTax;
	
	private JTextField _previousBalance;
	private JTextField _debitBalance;
	private JTextField _creditBalance;
	
	private IncomeTransactionController _incomeController;
	private OutcomeTranstactionController _outcomeController;
	
	private List<Transaction> _firstQuarterIncomeTransactions;
	private List<Transaction> _secondQuarterIncomeTransactions;
	private List<Transaction> _thirdQuarterIncomeTransactions;
	private List<Transaction> _fourthQuarterIncomeTransactions;
	
	private List<Transaction> _firstQuarterOutcomeTransactions;
	private List<Transaction> _secondQuarterOutcomeTransactions;
	private List<Transaction> _thirdQuarterOutcomeTransactions;
	private List<Transaction> _fourthQuarterOutcomeTransactions;
	
	
	
	
	private double[] _taxes;
	
	
	public VATForYieldDialog(double[] taxes) 
	{
		super("������������");
		_incomeController = new IncomeTransactionController();
		_outcomeController = new OutcomeTranstactionController();
		_taxes = taxes;
		initGUI();
	}
	
	public void initGUI()
	{
		this.setLocationRelativeTo(null);
		this.setSize(1000, 500);
		JPanel masterPanel = new JPanel(new BorderLayout());
		JPanel comboBoxPanel = new JPanel(new MigLayout());
		JPanel inputFieldsPanel = new JPanel(new MigLayout());
		JPanel outputFieldsPanel = new JPanel(new MigLayout());
		JPanel resultPanel = new JPanel(new MigLayout());
		
		
		comboBoxPanel.add(new JLabel("����"));
		_year = new JTextField(8);
		comboBoxPanel.add(_year);
		
		_select = new JButton("�������");
		
		_select.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(checkFields())
				{
					String year = _year.getText().toString();
					_firstQuarterIncomeTransactions = _incomeController.getTransactionIncomeByDate("01/01/" + year, "31/03/" + year);
					_secondQuarterIncomeTransactions = _incomeController.getTransactionIncomeByDate("01/04/" + year, "30/06/" + year);
					_thirdQuarterIncomeTransactions = _incomeController.getTransactionIncomeByDate("01/07/" + year, "30/09/" + year);
					_fourthQuarterIncomeTransactions = _incomeController.getTransactionIncomeByDate("01/10/" + year, "31/12/" + year);
					
					_firstQuarterOutcomeTransactions = _outcomeController.getTransactionIncomeByDate("01/01/" + year, "31/03/" + year);
					_secondQuarterOutcomeTransactions = _outcomeController.getTransactionIncomeByDate("01/04/" + year, "30/06/" + year);
					_thirdQuarterOutcomeTransactions = _outcomeController.getTransactionIncomeByDate("01/07/" + year, "30/09/" + year);
					_fourthQuarterOutcomeTransactions = _outcomeController.getTransactionIncomeByDate("01/10/" + year, "31/12/" + year);
					
					_periods.setEnabled(true);
				}
			}
		});
		
		comboBoxPanel.add(_select);
		
		
		
		//create periods
		_arrayPeriods = new String[]{"A' �������", "B' �������", "�' �������", "�' �������"};
		comboBoxPanel.add(new JLabel("�������"));
		_periods = new JComboBox(_arrayPeriods);
		_periods.setEnabled(false);
		comboBoxPanel.add(_periods);

		
		_execute = new JButton("��������");
		_execute.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(checkFields())
				{
					String selectedPeriod = (String) _periods.getSelectedItem();	
					if(selectedPeriod.equals("A' �������"))
					{
						//calculateAndDisplayFields(_firstQuarterIncomeTransactions, _firstQuarteOutcome....);
						calculateAndDisplayFields(_firstQuarterIncomeTransactions, _firstQuarterOutcomeTransactions);
						List<QuarterTransactionPair> list = new ArrayList<>();
						calculateAndDisplayBalance(list);
					}
					else if(selectedPeriod.equals("B' �������"))
					{
						System.out.println(_secondQuarterIncomeTransactions.size());
						calculateAndDisplayFields(_secondQuarterIncomeTransactions, _secondQuarterOutcomeTransactions);
						List<QuarterTransactionPair> list = new ArrayList<>();
						list.add(new QuarterTransactionPair(_firstQuarterIncomeTransactions, _firstQuarterOutcomeTransactions));
						calculateAndDisplayBalance(list);
					}
					else if(selectedPeriod.equals("�' �������"))
					{
						calculateAndDisplayFields(_thirdQuarterIncomeTransactions, _thirdQuarterOutcomeTransactions);
						List<QuarterTransactionPair> list = new ArrayList<>();
						list.add(new QuarterTransactionPair(_firstQuarterIncomeTransactions, _firstQuarterOutcomeTransactions));
						list.add(new QuarterTransactionPair(_secondQuarterIncomeTransactions, _secondQuarterOutcomeTransactions));
						calculateAndDisplayBalance(list);
					}
					else if(selectedPeriod.equals("�' �������"))
					{
						calculateAndDisplayFields(_fourthQuarterIncomeTransactions, _fourthQuarterOutcomeTransactions);
						List<QuarterTransactionPair> list = new ArrayList<>();
						list.add(new QuarterTransactionPair(_firstQuarterIncomeTransactions, _firstQuarterOutcomeTransactions));
						list.add(new QuarterTransactionPair(_secondQuarterIncomeTransactions, _secondQuarterOutcomeTransactions));
						list.add(new QuarterTransactionPair(_thirdQuarterIncomeTransactions, _thirdQuarterOutcomeTransactions));
						calculateAndDisplayBalance(list);
					}

				}
			}
		});
		
		comboBoxPanel.add(_execute);
		
		_clear = new JButton("����������");
		_clear.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{	
				clearFields();
			}
	
		});
		
		comboBoxPanel.add(_clear);
		
		
		// create intput panel
		inputFieldsPanel.add(new JLabel("������� 13%"));
		_inputsMin = new JTextField(10);
		_inputsMin.setEditable(false);
		inputFieldsPanel.add(_inputsMin);
		
		inputFieldsPanel.add(new JLabel("�.�.�. 13%"));
		_inputTaxMin = new JTextField(10);
		_inputTaxMin.setEditable(false);
		inputFieldsPanel.add(_inputTaxMin, "wrap");
		
		inputFieldsPanel.add(new JLabel("������� 23%"));
		_inputsMax = new JTextField(10);
		_inputsMax.setEditable(false);
		inputFieldsPanel.add(_inputsMax);
		
		inputFieldsPanel.add(new JLabel("�.�.�. 23%"));
		_inputTaxMax = new JTextField(10);
		_inputTaxMax.setEditable(false);
		inputFieldsPanel.add(_inputTaxMax, "wrap");
		
		inputFieldsPanel.add(new JLabel("������ �������"));
		_sumOfInputs = new JTextField(10);
		_sumOfInputs.setEditable(false);
		inputFieldsPanel.add(_sumOfInputs);
		
		inputFieldsPanel.add(new JLabel("������ �.�.�."));
		_sumOfInputTax = new JTextField(10);
		_sumOfInputTax.setEditable(false);
		inputFieldsPanel.add(_sumOfInputTax, "wrap");
		
		// create output panel
		outputFieldsPanel.add(new JLabel("������ 13%"));
		_outputsMin = new JTextField(10);
		_outputsMin.setEditable(false);
		outputFieldsPanel.add(_outputsMin);
		
		outputFieldsPanel.add(new JLabel("�.�.�. 13%"));
		_outputTaxMin = new JTextField(10);
		_outputTaxMin.setEditable(false);
		outputFieldsPanel.add(_outputTaxMin, "wrap");
		
		outputFieldsPanel.add(new JLabel("������ 23%"));
		_outputsMax = new JTextField(10);
		_outputsMax.setEditable(false);
		outputFieldsPanel.add(_outputsMax);
		
		outputFieldsPanel.add(new JLabel("�.�.�. 23%"));
		_outputTaxMax = new JTextField(10);
		_outputTaxMax.setEditable(false);
		outputFieldsPanel.add(_outputTaxMax, "wrap");
		
		outputFieldsPanel.add(new JLabel("������� ����� �������� ��������"));
		_outputWithoutTax = new JTextField(10);
		_outputWithoutTax.setEditable(false);
		outputFieldsPanel.add(_outputWithoutTax, "wrap");
		
		outputFieldsPanel.add(new JLabel("������ ������"));
		_sumOfOutputs = new JTextField(10);
		_sumOfOutputs.setEditable(false);
		outputFieldsPanel.add(_sumOfOutputs);
		
		outputFieldsPanel.add(new JLabel("������ �.�.�."));
		_sumOfOutputTax = new JTextField(10);
		_sumOfOutputTax.setEditable(false);
		outputFieldsPanel.add(_sumOfOutputTax, "wrap");
		
		// create output panel
		resultPanel.add(new JLabel("����������� ��������"));
		_previousBalance = new JTextField(10);
		_previousBalance.setEditable(false);
		resultPanel.add(_previousBalance);

		resultPanel.add(new JLabel("��������� ��������"));
		_debitBalance = new JTextField(10);
		_debitBalance.setEditable(false);
		resultPanel.add(_debitBalance);
		
		resultPanel.add(new JLabel("��������� ��������"));
		_creditBalance = new JTextField(10);
		_creditBalance.setEditable(false);
		resultPanel.add(_creditBalance);
		
		masterPanel.add(comboBoxPanel, BorderLayout.NORTH);
		masterPanel.add(inputFieldsPanel, BorderLayout.WEST);
		masterPanel.add(outputFieldsPanel, BorderLayout.EAST);
		masterPanel.add(resultPanel, BorderLayout.SOUTH);
		this.getContentPane().add(masterPanel);
	}
	
	private boolean checkFields()
	{
		//check year
		String year = _year.getText().toString();
		if(!Utilities.isYear(year))
		{
			JOptionPane.showMessageDialog(VATForYieldDialog.this, "������ �� �������� ����",  "��������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	private void calculateAndDisplayFields(List<Transaction> incomeTransactions, List<Transaction> outcomeTransactions)
	{
		TransactionCalculator calc = new TransactionCalculator(incomeTransactions, _taxes);
		
		calc.calculate();
		
		_inputsMin.setText(calc.get_smallAmountSum() + "");
		_inputsMax.setText(calc.get_largeAmountSum() + "");
		_sumOfInputs.setText(calc.get_totalAmount() + "");
		_inputTaxMin.setText(calc.get_smallTaxSum() + "");
		_inputTaxMax.setText(calc.get_largeTaxSum() + "");
		_sumOfInputTax.setText(calc.get_totalTax() + "");
		//outcome _
		
		calc = new TransactionCalculator(outcomeTransactions, _taxes);
		
		calc.calculate();

		_outputsMin.setText(calc.get_smallAmountSum() + "");
		_outputsMax.setText(calc.get_largeAmountSum() + "");
		_sumOfOutputs.setText(calc.get_totalAmount() + "");
		_outputTaxMin.setText(calc.get_smallTaxSum() + "");
		_outputTaxMax.setText(calc.get_largeTaxSum() + "");
		_sumOfOutputTax.setText(calc.get_totalTax() + "");
	}
	
	private void clearFields()
	{
		_inputsMin.setText("");
		_inputsMax.setText("");
		_sumOfInputs.setText("");
		_inputTaxMin.setText("");
		_inputTaxMax.setText("");
		_sumOfInputTax.setText("");
		
		_outputsMin.setText("");
		_outputsMax.setText("");
		_sumOfOutputs.setText("");
		_outputTaxMin.setText("");
		_outputTaxMax.setText("");
		_sumOfOutputTax.setText("");
	}
	
	private void calculateAndDisplayBalance(List<QuarterTransactionPair> list)
	{
		
		
		double thisQuarterIncomeTax = Double.parseDouble(_sumOfInputTax.getText().toString());
		double thisQuarterOutcomeTax = Double.parseDouble(_sumOfOutputTax.getText().toString());
		
		
		double thisQuarterTax = thisQuarterIncomeTax - thisQuarterOutcomeTax;
		double tempTax = 0.0;
		
		
		for(QuarterTransactionPair pair : list)
		{
			//calculate income 
			TransactionCalculator calc = new TransactionCalculator(pair.getIncomeTransactions(), _taxes);
			calc.calculate();
			tempTax += calc.get_totalTax();
			
			//caluclate outcome
			calc = new TransactionCalculator(pair.getOutcomeTransactions(), _taxes);
			calc.calculate();
			tempTax -= calc.get_totalTax();
		}
		
		_previousBalance.setText(tempTax + "");
		
		double overall = thisQuarterTax + tempTax;
		if(overall > 0)
			_creditBalance.setText(overall + "");
		else
		{
			String strOverall = Double.toString(overall);
			_debitBalance.setText(strOverall.substring(1, strOverall.length()));
		}
			
	}
	
	public class QuarterTransactionPair 
	{
		private List<Transaction> _incomeTransactions;
		private List<Transaction> _outcomeTransactions;
		
		public QuarterTransactionPair(List<Transaction> incomeTransactions, List<Transaction> outcomeTransactions)
		{
			_incomeTransactions = incomeTransactions;
			_outcomeTransactions = outcomeTransactions;
		}
		
		public List<Transaction> getIncomeTransactions()
		{
			return _incomeTransactions;
		}
		
		public List<Transaction> getOutcomeTransactions()
		{
			return _outcomeTransactions;
		}
	}
}
