package com.finance.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.finance.controller.CompanyController;
import com.finance.model.Company;
import com.finance.util.Utilities;

import net.miginfocom.swing.MigLayout;

public class CreateCompanyDialog
{
	private JFrame _parent;
	private JDialog _dialog;
	
	
	private JTextField _name;
	private JTextField _registryNumber; 
	private JTextField _expertise; 
	private JTextField _city; 
	private JTextField _address; 
	private JTextField _fax;
	private JTextField _title; 
	private JTextField _taxStore; 
	private JTextField _email; 
	private JTextField _zipCode; 
	private JTextField _phone;
	private JTextField _startDate;
	
	private JButton _confirmButton;
	private JButton _cancelButton;
	
	private CompanyController _controller; 
	
	private boolean _hasCompany;
	private String _companyName;
	
	public CreateCompanyDialog(JFrame parent)
	{
		_parent = parent;
		_hasCompany = false;
		_companyName = "";
	}
	
	private JPanel initPanel()
	{
		JPanel dialogPanel = new JPanel(new BorderLayout());
		JPanel fieldsPanel = new JPanel(new MigLayout());
		
		fieldsPanel.add(new JLabel("�����"));
		_name = new JTextField(20);
		fieldsPanel.add(_name);
		
		fieldsPanel.add(new JLabel("������"));
		_title = new JTextField(20);
		fieldsPanel.add(_title, "wrap");
		
		fieldsPanel.add(new JLabel("�.�.�."));
		_registryNumber = new JTextField(20);
		fieldsPanel.add(_registryNumber);
		
		fieldsPanel.add(new JLabel("�.�.�."));
		_taxStore = new JTextField(20);
		fieldsPanel.add(_taxStore, "wrap");
		
		fieldsPanel.add(new JLabel("����������� ��������"));
		_expertise = new JTextField(20);
		fieldsPanel.add(_expertise);
		
		fieldsPanel.add(new JLabel("E-mail"));
		_email = new JTextField(20);
		fieldsPanel.add(_email, "wrap");		
		
		fieldsPanel.add(new JLabel("����"));
		_city = new JTextField(20);
		fieldsPanel.add(_city);
		
		fieldsPanel.add(new JLabel("������������ �������"));
		_zipCode = new JTextField(20);
		fieldsPanel.add(_zipCode, "wrap");
		
		fieldsPanel.add(new JLabel("���������"));
		_address = new JTextField(20);
		fieldsPanel.add(_address);
		
		fieldsPanel.add(new JLabel("��������"));
		_phone = new JTextField(20);
		fieldsPanel.add(_phone, "wrap");
		
		fieldsPanel.add(new JLabel("Fax"));
		_fax = new JTextField(20);
		fieldsPanel.add(_fax);
		
		fieldsPanel.add(new JLabel("��/��� �������"));
		_startDate = new JTextField(20);
		fieldsPanel.add(_startDate);
		
		JPanel buttonPanel = new JPanel(new GridLayout());
		_confirmButton = new JButton("��");
		_confirmButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(checkFields())
				{
					CompanyController.createCompanyDB(_name.getText().toString());
					_controller = new CompanyController();
					Company company = new Company(1,
												 _name.getText().toString(),
												 _registryNumber.getText().toString(),
												 _expertise.getText().toString(),
												 _city.getText().toString(),
												 _address.getText().toString(),
												 _fax.getText().toString(),
												 _title.getText().toString(),
											 	 _taxStore.getText().toString(),
											 	 _email.getText().toString(),
										 	 	 _zipCode.getText().toString(), 
										 	 	 _phone.getText().toString(),
										 	 	 _startDate.getText().toString()	 
												);
					if(_controller.createCompany(company))
					{
						JOptionPane.showMessageDialog(_parent, "�������� ���������� ��������",  "���������", JOptionPane.INFORMATION_MESSAGE);
						_hasCompany = true;
						_companyName = _name.getText().toString();
					}
					else
					{
						JOptionPane.showMessageDialog(_parent, "���������� ���������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
						_hasCompany = false;
					}	
					_dialog.dispose();
				}
			}
			
		});
		_cancelButton = new JButton("�����");
		_cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				_dialog.dispose();
			}
			
		});
		
		buttonPanel.add(_confirmButton);
		buttonPanel.add(_cancelButton);
		
		dialogPanel.add(fieldsPanel, BorderLayout.CENTER);
		dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);
		return dialogPanel;
	}
	
	public void display()
	{
        final int DWIDTH = 500;
        final int DHEIGHT = 220;

        _dialog = new JDialog (_parent, "���������� ��������", true);
        _dialog.setSize(DWIDTH, DHEIGHT);
        _dialog.setResizable(false);
        _dialog.setDefaultCloseOperation (JDialog.DISPOSE_ON_CLOSE);

        _dialog.setContentPane(initPanel());

        _dialog.setLocationRelativeTo(_parent);
        _dialog.setVisible(true);
	}
	
	public boolean hasCompany()
	{
		return _hasCompany;
	}
	
	private boolean checkFields()
	{
		String name = _name.getText().toString();
		if(name.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		String registryNumber = _registryNumber.getText().toString();
		if(registryNumber.length() != 9)
		{
			JOptionPane.showMessageDialog(_parent, "�� ��� ������ �� ����� 9�����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if(!Utilities.isInteger(registryNumber))
		{
			JOptionPane.showMessageDialog(_parent, "�� ��� ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		String expertise = _expertise.getText().toString();
		if(expertise.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� ����������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String city = _city.getText().toString();
		if(city.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� ����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String address = _address.getText().toString();
		if(address.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� ���������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String fax = _fax.getText().toString();
		if(fax.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� fax",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isPhone(fax))
		{
			JOptionPane.showMessageDialog(_parent, "�� fax ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String title = _title.getText().toString();
		if(title.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� �����",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String taxStore = _taxStore.getText().toString();
		if(taxStore.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� �.�.�.",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String email = _email.getText().toString();
		if(email.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� email",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String zipCode = _zipCode.getText().toString();
		if(zipCode.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� ����������� ������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isInteger(zipCode))
		{
			JOptionPane.showMessageDialog(_parent, "� ������������ ������� ������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String phone = _phone.getText().toString();
		if(phone.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� ��������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if(!Utilities.isPhone(phone))
		{
			JOptionPane.showMessageDialog(_parent, "�� �������� ������ �� ����� ��� ������ +30-����������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		String startDate = _startDate.getText().toString();
		if(startDate.isEmpty())
		{
			JOptionPane.showMessageDialog(_parent, "������ �� �������� ���������� �������",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(!Utilities.isDate(startDate))
		{
			JOptionPane.showMessageDialog(_parent, "� ���������� ������ �� ����� ��� ������ dd/mm/yyyy",  "������", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
	
	public String getCompanyName()
	{
		return _companyName;
	}
}
