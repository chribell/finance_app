package com.finance.db;

import java.util.List;

import com.finance.model.Account;

public interface AccountDAO 
{
	List<Account> getAll();
	Account getByID(int id);
	Account getByCode(String code);
	Account getByRegistryNumber(String registry_num);
	boolean insertAccount(Account account);
	boolean updateAccount(Account account);
	boolean deleteAccount(int id);
}
