package com.finance.db;

import java.util.List;

import com.finance.model.Category;

public interface CategoryDAO 
{
	List<Category> getAll();
	Category getByID(int id);
	boolean insertCategory(Category category);
	boolean updateCategory(Category category);
}
