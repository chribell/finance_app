package com.finance.db;

public class DatabaseException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8667268557838822005L;
	
	public DatabaseException(String message)
	{
		super(message);
	}
	
	public DatabaseException(Throwable cause)
	{
		super(cause);
	}

	public DatabaseException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public DatabaseException(Exception e)
	{
		super(e);
	}

}
