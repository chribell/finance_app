package com.finance.db;

import java.util.List;

import com.finance.model.Document;

public interface DocumentDAO 
{
	List<Document> getAll();
	Document getByID(int id);
	boolean insertDocument(Document document);
	boolean updateDocument(Document document);
}
