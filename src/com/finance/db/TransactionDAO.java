package com.finance.db;

import java.util.List;

import com.finance.model.Transaction;

public interface TransactionDAO 
{
	List<Transaction> getAll();
	List<Transaction> getByDate(String startDate, String endDate);
	boolean insertTransaction(Transaction transaction);
	boolean deleteTransaction(int id);
	Transaction getTransaction(int id);
}
