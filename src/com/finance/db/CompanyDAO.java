package com.finance.db;

import java.util.List;

import com.finance.model.Company;

public interface CompanyDAO 
{
    List<Company> getAll();
    Company getByID(int id);
    Company getLastCompany();
    boolean insertCompany(Company company);
    boolean updateCompany(Company company);
    boolean deleteCompany(Company company);
}
