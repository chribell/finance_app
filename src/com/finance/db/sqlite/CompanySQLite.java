package com.finance.db.sqlite;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.finance.db.CompanyDAO;
import com.finance.db.DatabaseException;
import com.finance.model.Company;
import com.finance.util.Utilities;

public class CompanySQLite implements CompanyDAO
{
	private SQLiteSingleton _sqlite;
	private Statement _stmt;

	public CompanySQLite()
	{
		try 
		{
			_sqlite = SQLiteSingleton.getInstance();
		} 
		catch (DatabaseException e) 
		{
			System.err.println(e);
		}
	}
	
	@Override
	public List<Company> getAll() 
	{
		List<Company> companies = new ArrayList<>();
		try
		{
			_stmt = _sqlite.createStatement();
			ResultSet resultSet = _stmt.executeQuery("SELECT * FROM COMPANY;");
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String registryNumber = resultSet.getString("registry_num");
				String expertise = resultSet.getString("expertise");
				String city = resultSet.getString("city");
				String address = resultSet.getString("address");
				String fax = resultSet.getString("fax");
				String title = resultSet.getString("title");
				String taxStore = resultSet.getString("tax_store");
				String email = resultSet.getString("email");
				String zipCode = resultSet.getString("zip_code");
				String phone = resultSet.getString("phone");
				String startDate = resultSet.getString("startdate");
				companies.add(new Company(id, name, registryNumber, expertise, city, address, fax, title, taxStore, email, zipCode, phone, startDate));
			}
		}
		catch(SQLException e)
		{
			
		}
		return companies;
	}

	@Override
	public Company getByID(int id) 
	{
		Company company = null;
		try 
		{
			PreparedStatement prepStmt = _sqlite.createPreparedStatement("SELECT * FROM COMPANY WHERE id = ?");
			prepStmt.setInt(1, id);
			ResultSet resultSet = prepStmt.executeQuery();
			if (resultSet.isBeforeFirst()) 
			{    
				String name = resultSet.getString("name");
				String registryNumber = resultSet.getString("registry_num");
				String expertise = resultSet.getString("expertise");
				String city = resultSet.getString("city");
				String address = resultSet.getString("address");
				String fax = resultSet.getString("fax");
				String title = resultSet.getString("title");
				String taxStore = resultSet.getString("tax_store");
				String email = resultSet.getString("email");
				String zipCode = resultSet.getString("zip_code");
				String phone = resultSet.getString("phone");
				String startDate = resultSet.getString("startdate");
				company = new Company(id, name, registryNumber, expertise, city, address, fax, title, taxStore, email, zipCode, phone, startDate);
			} 
		} 
		catch (SQLException e) 
		{
			
		}
		return company;
	}

	@Override
	public boolean insertCompany(Company company) 
	{
		try 
		{
			String query = "INSERT INTO COMPANY (id, name, registry_num, expertise, city, address, fax, title, tax_store, email, zip_code, phone, startdate) "
						 + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement prepStmt = _sqlite.createPreparedStatement(query);
			prepStmt.setInt(1, company.get_id());
			prepStmt.setString(2, company.get_name());
			prepStmt.setString(3, company.get_registryNumber());
			prepStmt.setString(4, company.get_expertise());
			prepStmt.setString(5, company.get_city());
			prepStmt.setString(6, company.get_address());
			prepStmt.setString(7, company.get_fax());
			prepStmt.setString(8, company.get_title());
			prepStmt.setString(9, company.get_taxStore());
			prepStmt.setString(10, company.get_email());
			prepStmt.setString(11, company.get_zipCode());
			prepStmt.setString(12, company.get_phone());
			prepStmt.setString(13, Utilities.parseDate(company.get_startDate()));
			int res = prepStmt.executeUpdate();
			return res < 0 ? false : true;
		} 
		catch (SQLException e) 
		{
			System.out.println(e);
		}
		return false;
	}

	@Override
	public boolean updateCompany(Company company) {
		try 
		{
			String query = "UPDATE COMPANY SET name = ?, SET registry_num = ?, SET expertise = ?, SET city = ?, "
					+ "SET address = ?, SET fax = ?, title = ?, SET taxStore = ?, SET email = ?, "
					+ "SET zip_code = ?, SET phone = ?, SET startdate = ? WHERE ID = ?)";
			PreparedStatement prepStmt = _sqlite.createPreparedStatement(query);
			prepStmt.setString(1, company.get_name());
			prepStmt.setString(2, company.get_registryNumber());
			prepStmt.setString(3, company.get_expertise());
			prepStmt.setString(4, company.get_city());
			prepStmt.setString(5, company.get_address());
			prepStmt.setString(6, company.get_fax());
			prepStmt.setString(7, company.get_title());
			prepStmt.setString(8, company.get_taxStore());
			prepStmt.setString(9, company.get_email());
			prepStmt.setString(10, company.get_zipCode());
			prepStmt.setString(11, company.get_phone());
			prepStmt.setString(12, company.get_startDate());
			prepStmt.setInt(13, company.get_id());
			int res = prepStmt.executeUpdate();
			return res < 0 ? false : true;
		} 
		catch (SQLException e) 
		{
		}
		return false;
	}

	@Override
	public boolean deleteCompany(Company company) {
		// TODO Auto-generated method stub
		return false;
	}

	
	@Override
	public Company getLastCompany()
	{
		try 
		{
			String query = "SELECT * FROM COMPANY WHERE ID = (SELECT MAX(ID) FROM COMPANY);";
			_stmt = _sqlite.createStatement();
			ResultSet resultSet = _stmt.executeQuery(query);
			if(resultSet.isBeforeFirst()) 
			{    
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String registryNumber = resultSet.getString("registry_num");
				String expertise = resultSet.getString("expertise");
				String city = resultSet.getString("city");
				String address = resultSet.getString("address");
				String fax = resultSet.getString("fax");
				String title = resultSet.getString("title");
				String taxStore = resultSet.getString("tax_store");
				String email = resultSet.getString("email");
				String zipCode = resultSet.getString("zip_code");
				String phone = resultSet.getString("phone");
				String startDate = resultSet.getString("startdate");
				return new Company(id, name, registryNumber, expertise, city, address, fax, title, taxStore, email, zipCode, phone, startDate);
			} 
		} 
		catch (SQLException e) 
		{
			
		}
		return null;
		
	}
}
