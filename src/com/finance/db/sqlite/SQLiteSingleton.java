package com.finance.db.sqlite;

import java.io.File;
import java.sql.*;

import com.finance.db.DatabaseException;

public class SQLiteSingleton
{
	private static Connection _cnx;
	private static SQLiteSingleton _instance;
	private static String _dbPath;
	
	
	public static void setDatabasePath(String path)
	{
		_dbPath = path;
		_instance = null;
	}
	
	public static SQLiteSingleton getInstance() throws DatabaseException
	{
		if(_instance == null)
		{
			File db = new File(_dbPath);
			//if database doesn't exist 
			if(!db.exists())
			{
				_instance = new SQLiteSingleton(_dbPath, false);
				initDB();
			}
			else
			{
				_instance = new SQLiteSingleton(_dbPath, true);
			}
		}
		return _instance;
	}
	
	public static void close() throws DatabaseException
	{
		try 
		{
			if(_instance != null)
			{
				_cnx.close();
				_instance = null;
			}
		} 
		catch (SQLException e) 
		{
			throw new DatabaseException(e);
		}
	}
	
	private SQLiteSingleton(String dbPath, boolean exists) throws DatabaseException 
	{    
		try 
		{
			Class.forName("org.sqlite.JDBC");
			_cnx = DriverManager.getConnection("jdbc:sqlite:" + dbPath + (exists ? "" : ".db"));
		} 
		catch (ClassNotFoundException e) 
		{
			throw new DatabaseException(e);
		} 
		catch (SQLException e)
		{
			throw new DatabaseException(e);
		}
	}
	
	private static void initDB() throws DatabaseException
	{
		try 
		{
			Statement stmt = _cnx.createStatement();
			
			//create company table
			String companyTable =  "CREATE TABLE COMPANY " +
     	  		   				   "(ID INTEGER PRIMARY KEY   AUTOINCREMENT, " +
                                   " NAME           TEXT     NOT NULL, " +
                                   " REGISTRY_NUM   CHAR(9)  NOT NULL UNIQUE, " +
                                   " EXPERTISE      TEXT     NOT NULL, " +
                                   " CITY           TEXT     NOT NULL, " +
                                   " ADDRESS        TEXT     NOT NULL, " +
                                   " FAX            TEXT     NOT NULL UNIQUE, " +
                                   " TITLE          TEXT     NOT NULL, " +
                                   " TAX_STORE      TEXT     NOT NULL, " +
                                   " EMAIL          TEXT     NOT NULL UNIQUE, " +
                                   " ZIP_CODE       CHAR(5)  NOT NULL, " +
                                   " PHONE          CHAR(13) NOT NULL UNIQUE, " +
                                   " STARTDATE      TEXT     NOT NULL)";
			stmt.executeUpdate(companyTable);
			
			//create client table
			String clientTable =   "CREATE TABLE CLIENT " +
								   "(ID INTEGER PRIMARY KEY   AUTOINCREMENT, " +
								   " CODE           CHAR(11) NOT NULL UNIQUE, " +
								   " NAME  		    TEXT     NOT NULL, " +
								   " INCHARGE       TEXT     , " +    //only if, it is company, is not null
								   " OCCUPATION     TEXT     NOT NULL, " +
								   " CITY           TEXT     NOT NULL, " +
								   " ZIP_CODE       CHAR(5)  NOT NULL, " +
								   " REGISTRY_NUM   CHAR(9)  NOT NULL UNIQUE, " +
								   " TAX_STORE      TEXT     NOT NULL, " +
								   " ADDRESS        TEXT     NOT NULL, " +
								   " PHONE          CHAR(13) NOT NULL UNIQUE) ";
			stmt.executeUpdate(clientTable);
			
			//create supplier table
			String supplierTable = "CREATE TABLE SUPPLIER " +
								   "(ID INTEGER PRIMARY KEY   AUTOINCREMENT, " +
								   " CODE           CHAR(11) NOT NULL UNIQUE, " +
								   " NAME  		    TEXT     NOT NULL, " +
								   " INCHARGE       TEXT    , " +    //only if, it is company, is not null
								   " OCCUPATION     TEXT     NOT NULL, " +
								   " CITY           TEXT     NOT NULL, " +
								   " ZIP_CODE       CHAR(5)  NOT NULL, " +
								   " REGISTRY_NUM   CHAR(9)  NOT NULL UNIQUE, " +
								   " TAX_STORE      TEXT     NOT NULL, " +
								   " ADDRESS        TEXT     NOT NULL, " +
								   " PHONE          CHAR(13) NOT NULL UNIQUE)";
			stmt.executeUpdate(supplierTable);
		

			
			//create document income table
			String docIncTable =  "CREATE TABLE DOCUMENT_INCOME " +
	   				   			  "(ID INTEGER PRIMARY KEY   AUTOINCREMENT, " +
	   				   			  " NAME           TEXT      NOT NULL, " +
	   				   			  " ABBREVIATION   TEXT      NOT NULL UNIQUE, " +
	   				   			  " REQUIRE_RN     INT       NOT NULL)";
			stmt.executeUpdate(docIncTable);
			
			//create document outcome table
			String docOutTable =  "CREATE TABLE DOCUMENT_OUTCOME " +
	   				   			  "(ID INTEGER PRIMARY KEY   AUTOINCREMENT, " +
	   				   			  " NAME           TEXT      NOT NULL, " +
	   				   			  " ABBREVIATION   TEXT      NOT NULL UNIQUE, " +
	   				   			  " REQUIRE_RN     INT       NOT NULL) ";
			stmt.executeUpdate(docOutTable);
			
			//create category income table
			String catInTable =   "CREATE TABLE CATEGORY_INCOME " +
	   				   			  "(ID INTEGER PRIMARY KEY   AUTOINCREMENT, " +
	   				   			  " CODE           CHAR(11)  NOT NULL UNIQUE, " +
	   				   			  " EXPLANATION    TEXT      NOT NULL, " +
	   				   			  " FIELDS         INT       NOT NULL) ";
			stmt.executeUpdate(catInTable);
			
			//create category outcome table
			String catOutTable =  "CREATE TABLE CATEGORY_OUTCOME " +
	   				   			  "(ID INTEGER PRIMARY KEY   AUTOINCREMENT, " +
	   				   			  " CODE           CHAR(11)  NOT NULL UNIQUE, " +
	   				   			  " EXPLANATION    TEXT      NOT NULL, " +
	   				   			  " FIELDS         INT       NOT NULL)";
			stmt.executeUpdate(catOutTable);
			
			
			//create income table
			String incomeTable =   "CREATE TABLE INCOME " +
     	  		   				   "(ID INTEGER PRIMARY KEY   AUTOINCREMENT, " +
     	  		   				   " TYPE             INT      NOT NULL, " +
                                   " DATE             TEXT     NOT NULL, " +
     	  		   				   " DOCUMENT         INT      NOT NULL, " + 
                                   " CLIENT_CODE      INT      , " +
                                   " CATEGORY         INT      NOT NULL, " +
                                   " AMOUNT          DOUBLE     	   , " +
                                   " FOREIGN KEY(DOCUMENT) REFERENCES  DOCUMENT_INCOME(ID), " +
                                   " FOREIGN KEY(CLIENT_CODE) REFERENCES CLIENT(ID), " +
                                   " FOREIGN KEY(CATEGORY) REFERENCES CATEGORY_INCOME(ID) " +
								   ")";
			stmt.executeUpdate(incomeTable);
			
			//create outcome table
			String outcomeTable =  "CREATE TABLE OUTCOME " +
     	  		   				   "(ID INTEGER PRIMARY KEY   AUTOINCREMENT, " +
     	  		   				   " TYPE             INT      NOT NULL, " +
                                   " DATE             TEXT     NOT NULL, " +
     	  		   				   " DOCUMENT         INT      NOT NULL, " + 
                                   " SUPPLIER_CODE    INT      , " +
                                   " CATEGORY         INT      NOT NULL, " +
                                   " AMOUNT          DOUBLE     	   , " +
                                   " FOREIGN KEY(DOCUMENT) REFERENCES  DOCUMENT_OUTCOME(ID), " +
                                   " FOREIGN KEY(SUPPLIER_CODE)  REFERENCES SUPPLIER(ID), " +
                                   " FOREIGN KEY(CATEGORY) REFERENCES CATEGORY_OUTCOME(ID) " +
								   ")";
			stmt.executeUpdate(outcomeTable);
			stmt.close();
			

			
		} 
		catch (SQLException e) 
		{
			System.err.println(e);
			throw new DatabaseException(e);
		}
	}
	
	
	
	public Statement createStatement() throws SQLException
	{
		return _cnx.createStatement();
	}
	
	public PreparedStatement createPreparedStatement(String sql) throws SQLException
	{
		return _cnx.prepareStatement(sql);
	}	
	

	
	public void executeSQL(String query) throws DatabaseException
	{
		try 
		{
			Statement stmt = _cnx.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
		} 
		catch(SQLException e) 
		{
			throw new DatabaseException(e);
		}
	}
	
	
}