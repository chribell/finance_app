package com.finance.db.sqlite;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.finance.db.CategoryDAO;
import com.finance.db.DatabaseException;
import com.finance.model.Account;
import com.finance.model.Category;

public class CategoryIncomeSQLite implements CategoryDAO 
{
	private SQLiteSingleton _sqlite;
	private Statement _stmt;

	public CategoryIncomeSQLite()
	{
		try 
		{
			_sqlite = SQLiteSingleton.getInstance();
		} 
		catch (DatabaseException e) 
		{
			System.err.println(e);
		}
	}
	
	@Override
	public List<Category> getAll() 
	{
		List<Category> categories = new ArrayList<>();
		try
		{
			_stmt = _sqlite.createStatement();
			ResultSet resultSet = _stmt.executeQuery("SELECT * FROM CATEGORY_INCOME;");
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String code = resultSet.getString("code");
				String explanation = resultSet.getString("explanation");
				int numOfFields = resultSet.getInt("fields");
				categories.add(new Category(id, code, explanation, numOfFields));
			}
		}
		catch(SQLException e)
		{
			System.err.println(e);
		}
		return categories;
	}

	@Override
	public Category getByID(int id)
	{
		Category category = null;
		try 
		{
			PreparedStatement prepStmt = _sqlite.createPreparedStatement("SELECT * FROM CATEGORY_INCOME WHERE id = ?");
			prepStmt.setInt(1, id);
			ResultSet resultSet = prepStmt.executeQuery();
			if (resultSet.isBeforeFirst()) 
			{    
				String code = resultSet.getString("code");
				String explanation = resultSet.getString("explanation");
				int numOfFields = resultSet.getInt("fields");
				category = new Category(id, code, explanation, numOfFields);
			}
			
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		
		return category;
	}

	@Override
	public boolean insertCategory(Category category) 
	{
		try 
		{
			String query = "INSERT INTO CATEGORY_INCOME (code, explanation, fields) "
					 + "VALUES (?, ?, ?)";
			PreparedStatement prepStmt = _sqlite.createPreparedStatement(query);
			//prepStmt.setInt(1, category.get_id());
			prepStmt.setString(1, category.get_code());
			prepStmt.setString(2, category.get_explanation());
			prepStmt.setInt(3, category.get_numberOfFields());
			int res = prepStmt.executeUpdate();
			return res < 0 ? false : true;
		} 
		catch (SQLException e) 
		{
			System.err.println(e);
		}
		return false;
	}

	@Override
	public boolean updateCategory(Category category) 
	{
		try 
		{
			String query = "UPDATE CATEOGORY_INCOME SET explanation = ?, fields = ? WHERE code = ?)";
			PreparedStatement prepStmt = _sqlite.createPreparedStatement(query);
			prepStmt.setString(1, category.get_explanation());
			prepStmt.setInt(2, category.get_numberOfFields());
			prepStmt.setString(3, category.get_code());
			int res = prepStmt.executeUpdate();
			return res < 0 ? false : true;
		} 
		catch (SQLException e) 
		{
			System.err.println(e);
		}
		return false;
	}
	
	public Category getByCode(String code) 
	{
		Category category= null;
		try
		{
			PreparedStatement prepStmt = _sqlite.createPreparedStatement("SELECT * FROM CATEGORY_INCOME WHERE code = ?");
			prepStmt.setString(1, code);
			ResultSet resultSet = prepStmt.executeQuery();
			if (resultSet.isBeforeFirst()) 
			{
				int id = resultSet.getInt("id");
				String explanation = resultSet.getString("explanation");
				int numOfFields = resultSet.getInt("fields");
				category = new Category(id, code, explanation, numOfFields);
			}
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		return category;
	}

}
