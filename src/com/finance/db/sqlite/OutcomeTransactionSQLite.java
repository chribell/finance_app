package com.finance.db.sqlite;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.finance.db.DatabaseException;
import com.finance.db.TransactionDAO;
import com.finance.model.Account;
import com.finance.model.Category;
import com.finance.model.Document;
import com.finance.model.Transaction;
import com.finance.util.Utilities;

public class OutcomeTransactionSQLite implements TransactionDAO 
{
	
	private SQLiteSingleton _sqlite;
	private Statement _stmt;
	
	public OutcomeTransactionSQLite() 
	{
		try 
		{
			_sqlite = SQLiteSingleton.getInstance();
		} 
		catch (DatabaseException e) 
		{
			System.err.println(e);
		}
	}

	@Override
	public List<Transaction> getAll() {
		List<Transaction> transactions = new ArrayList<>();
		try
		{	
			_stmt = _sqlite.createStatement();
			ResultSet resultSet = _stmt.executeQuery("SELECT * FROM OUTCOME;");
			while(resultSet.next()) 
			{
				int id = resultSet.getInt("id");
				int type = resultSet.getInt("type");
				String date = resultSet.getString("date");
				int documentID = resultSet.getInt("document");
				int supplierID = resultSet.getInt("supplier_code");
				if(resultSet.wasNull())
				{
					supplierID = -1;
				}
				int categoryID = resultSet.getInt("category");
				double amount = resultSet.getDouble("amount");
				
				DocumentOutcomeSQLite documentSQLite = new DocumentOutcomeSQLite();
				Document document = documentSQLite.getByID(documentID);
				
				CategoryOutcomeSQLite categorySQLite = new CategoryOutcomeSQLite();
				Category category = categorySQLite.getByID(categoryID);
				
				Transaction transaction = new Transaction(id, type, date, category, document, amount);
				
				if(supplierID != -1)
				{
					SupplierSQLite supplierSQLite = new SupplierSQLite();
					Account supplier = supplierSQLite.getByID(supplierID);
					transaction.setAccount(supplier);
				}
								
				transactions.add(transaction);
			}
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		return transactions;
	}

	@Override
	public List<Transaction> getByDate(String startDate, String endDate) 
	{
		List<Transaction> transactions = new ArrayList<>();
		try
		{	
			PreparedStatement prepStmt = _sqlite.createPreparedStatement("SELECT * FROM OUTCOME WHERE date BETWEEN ? AND ?");
			prepStmt.setString(1, startDate);
			prepStmt.setString(2, endDate);
			ResultSet resultSet = prepStmt.executeQuery();
			while(resultSet.next()) 
			{
				int id = resultSet.getInt("id");
				int type = resultSet.getInt("type");
				String date = resultSet.getString("date");
				int documentID = resultSet.getInt("document");
				int supplierID = resultSet.getInt("supplier_code");
				if(resultSet.wasNull())
				{
					supplierID = -1;
				}
				int categoryID = resultSet.getInt("category");
				double amount = resultSet.getDouble("amount");
				
				DocumentOutcomeSQLite documentSQLite = new DocumentOutcomeSQLite();
				Document document = documentSQLite.getByID(documentID);
				
				CategoryOutcomeSQLite categorySQLite = new CategoryOutcomeSQLite();
				Category category = categorySQLite.getByID(categoryID);
				
				Transaction transaction = new Transaction(id, type, date, category, document, amount);
				
				if(supplierID != -1)
				{
					SupplierSQLite supplierSQLite = new SupplierSQLite();
					Account supplier = supplierSQLite.getByID(supplierID);
					transaction.setAccount(supplier);
				}
								
				transactions.add(transaction);
			}
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		return transactions;
	}

	@Override
	public boolean insertTransaction(Transaction transaction) {
		try 
		{
			String query = "INSERT INTO OUTCOME (type, date, document, supplier_code, category, amount) "
						 + "VALUES (?, ?, ?, ?, ?, ?)";
			PreparedStatement prepStmt = _sqlite.createPreparedStatement(query);
			//prepStmt.setInt(1, transaction.getID());
			prepStmt.setInt(1, transaction.getTypeID());
			prepStmt.setString(2, Utilities.parseDate(transaction.getDate()));
			prepStmt.setInt(3, transaction.getDocument().get_id());
			if(transaction.getAccount() == null) //if the transaction has no corresponding account
				prepStmt.setNull(4, Types.NULL);
			else
				prepStmt.setInt(4, transaction.getAccount().get_id());
			prepStmt.setInt(5, transaction.getCategory().get_id());
			prepStmt.setDouble(6, transaction.getAmount());

			int res = prepStmt.executeUpdate();
			return res < 0 ? false : true;
		} 
		catch (SQLException e) 
		{
			System.err.println(e);
		}
		return false;
	}

	@Override
	public boolean deleteTransaction(int id) 
	{
		try
		{
			 PreparedStatement prepStmt = _sqlite.createPreparedStatement("DELETE FROM OUTCOME WHERE id = ?");
			 prepStmt.setInt(1, id);
			 int result = prepStmt.executeUpdate();
			 return result == 1 ? true : false; 
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		return false;
	}
	
	@Override
	public Transaction getTransaction(int id) 
	{
		Transaction transaction = null;
		try
		{
			PreparedStatement prepStmt = _sqlite.createPreparedStatement("SELECT * FROM OUTCOME WHERE id = ?");
			prepStmt.setInt(1, id);
			ResultSet resultSet = prepStmt.executeQuery();
			if (resultSet.isBeforeFirst()) 
			{
				int type = resultSet.getInt("type");
				String date = resultSet.getString("date");
				int documentID = resultSet.getInt("document");
				int supplierID = resultSet.getInt("supplier_code");
				if(resultSet.wasNull())
				{
					supplierID = -1;
				}
				int categoryID = resultSet.getInt("category");
				double amount = resultSet.getDouble("amount");
				
				DocumentOutcomeSQLite documentSQLite = new DocumentOutcomeSQLite();
				Document document = documentSQLite.getByID(documentID);
				
				CategoryOutcomeSQLite categorySQLite = new CategoryOutcomeSQLite();
				Category category = categorySQLite.getByID(categoryID);
				
				transaction = new Transaction(id, type, date, category, document, amount);
				
				if(supplierID != -1)
				{
					SupplierSQLite supplierSQLite = new SupplierSQLite();
					Account client = supplierSQLite.getByID(supplierID);
					transaction.setAccount(client);
				}
								
			}
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		return transaction;
	}
}
