package com.finance.db.sqlite;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.finance.db.AccountDAO;
import com.finance.db.DatabaseException;
import com.finance.model.Account;

public class ClientSQLite implements AccountDAO 
{
	private SQLiteSingleton _sqlite;
	private Statement _stmt;
	
	

	public ClientSQLite() 
	{
		try 
		{
			_sqlite = SQLiteSingleton.getInstance();
		} 
		catch (DatabaseException e) 
		{
			System.err.println(e);
		}
	}

	@Override
	public List<Account> getAll() {
		List<Account> accounts = new ArrayList<>();
		try
		{
			_stmt = _sqlite.createStatement();
			ResultSet resultSet = _stmt.executeQuery("SELECT * FROM CLIENT");
			while(resultSet.next()) 
			{
				int id = resultSet.getInt("id");
				String code = resultSet.getString("code");
				String name = resultSet.getString("name");
				String inCharge = resultSet.getString("inCharge");
				String occupation = resultSet.getString("occupation");
				String city = resultSet.getString("city");
				String zip_code = resultSet.getString("zip_code");
				String registry_num = resultSet.getString("registry_num");
				String tax_store = resultSet.getString("tax_store");
				String address = resultSet.getString("address");
				String phone = resultSet.getString("phone");
				accounts.add(new Account(id, code, name, inCharge, occupation, city, zip_code, registry_num, tax_store, address, phone));
			}
		}
		catch(SQLException e) 
		{			
			System.err.println(e);
		}
		return accounts;
	}

	@Override
	public Account getByID(int id) 
	{
		Account account = null;
		try
		{
			PreparedStatement prepStmt = _sqlite.createPreparedStatement("SELECT * FROM CLIENT WHERE id = ?");
			prepStmt.setInt(1, id);
			ResultSet resultSet = prepStmt.executeQuery();
			if (resultSet.isBeforeFirst()) 
			{
				String code = resultSet.getString("code");
				String name = resultSet.getString("name");
				String inCharge = resultSet.getString("inCharge");
				String occupation = resultSet.getString("occupation");
				String city = resultSet.getString("city");
				String zip_code = resultSet.getString("zip_code");
				String registry_num = resultSet.getString("registry_num");
				String tax_store = resultSet.getString("tax_store");
				String address = resultSet.getString("address");
				String phone = resultSet.getString("phone");
				account = new Account(id, code, name, inCharge, occupation, city, zip_code, registry_num, tax_store, address, phone);
			}
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		return account;
	}

	@Override
	public Account getByRegistryNumber(String registry_num) {
		Account account = null;
		try
		{
			PreparedStatement prepStmt = _sqlite.createPreparedStatement("SELECT * FROM CLIENT WHERE registry_num = ?");
			prepStmt.setString(1, registry_num);
			ResultSet resultSet = prepStmt.executeQuery();
			if (resultSet.isBeforeFirst()) 
			{
				int id = resultSet.getInt("id");
				String code = resultSet.getString("code");
				String name = resultSet.getString("name");
				String inCharge = resultSet.getString("inCharge");
				String occupation = resultSet.getString("occupation");
				String city = resultSet.getString("city");
				String zip_code = resultSet.getString("zip_code");
				String tax_store = resultSet.getString("tax_store");
				String address = resultSet.getString("address");
				String phone = resultSet.getString("phone");
				account = new Account(id, code, name, inCharge, occupation, city, zip_code, registry_num, tax_store, address, phone);
			}
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		return account;
	}

	@Override
	public boolean insertAccount(Account account) {
		try 
		{
			String query = "INSERT INTO CLIENT (code, name, incharge, occupation, city, zip_code, registry_num, tax_store, address, phone) "
					 + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement prepStmt = _sqlite.createPreparedStatement(query);
			//prepStmt.setInt(1, account.get_id());
			prepStmt.setString(1, account.get_code());
			prepStmt.setString(2, account.get_name());
			prepStmt.setString(3, account.get_inCharge());
			prepStmt.setString(4, account.get_occupation());
			prepStmt.setString(5, account.get_city());
			prepStmt.setString(6, account.get_zipCode());
			prepStmt.setString(7, account.get_registryNumber());
			prepStmt.setString(8, account.get_taxStore());
			prepStmt.setString(9, account.get_address());
			prepStmt.setString(10, account.get_phone());
			int res = prepStmt.executeUpdate();
			return res == 0 ? false : true;
		} 
		catch (SQLException e) 
		{
			System.err.println(e);
		}
		return false;
	}

	@Override
	public boolean updateAccount(Account account) 
	{
		try 
		{
			String query = "UPDATE CLIENT SET name = ?, incharge = ?, occupation = ?, "
					+ "city = ?, zip_code = ?, registry_num = ?, tax_store = ?, address = ?, "
					+ "phone = ? WHERE code = ?";
			PreparedStatement prepStmt = _sqlite.createPreparedStatement(query);
			prepStmt.setString(1, account.get_name());
			prepStmt.setString(2, account.get_inCharge());
			prepStmt.setString(3, account.get_occupation());
			prepStmt.setString(4, account.get_city());
			prepStmt.setString(5, account.get_zipCode());
			prepStmt.setString(6, account.get_registryNumber());
			prepStmt.setString(7, account.get_taxStore());
			prepStmt.setString(8, account.get_address());
			prepStmt.setString(9, account.get_phone());
			prepStmt.setString(10, account.get_code());
			int res = prepStmt.executeUpdate();
			return res == 0 ? false : true;
		} 
		catch (SQLException e) 
		{
			System.err.println(e);
		}
		return false;
	}

	@Override
	public boolean deleteAccount(int id) {
		try
		{
			 PreparedStatement prepStmt = _sqlite.createPreparedStatement("DELETE FROM CLIENT WHERE id = ?");
			 prepStmt.setInt(1, id);
			 int result = prepStmt.executeUpdate();
			 return result == 1 ? true : false; 
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		return false;
	}

	@Override
	public Account getByCode(String code) 
	{
		Account account = null;
		try
		{
			PreparedStatement prepStmt = _sqlite.createPreparedStatement("SELECT * FROM CLIENT WHERE code = ?");
			prepStmt.setString(1, code);
			ResultSet resultSet = prepStmt.executeQuery();
			if (resultSet.isBeforeFirst()) 
			{
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String inCharge = resultSet.getString("inCharge");
				String occupation = resultSet.getString("occupation");
				String city = resultSet.getString("city");
				String zip_code = resultSet.getString("zip_code");
				String registry_num = resultSet.getString("registry_num");
				String tax_store = resultSet.getString("tax_store");
				String address = resultSet.getString("address");
				String phone = resultSet.getString("phone");
				account = new Account(id, code, name, inCharge, occupation, city, zip_code, registry_num, tax_store, address, phone);
			}
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		return account;
	}

}
