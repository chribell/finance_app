package com.finance.db.sqlite;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.finance.db.DatabaseException;
import com.finance.db.DocumentDAO;
import com.finance.model.Category;
import com.finance.model.Document;

public class DocumentIncomeSQLite implements DocumentDAO  
{
	private Statement _stmt;
	private SQLiteSingleton _sqlite;
	
	public DocumentIncomeSQLite()
	{
		try 
		{
			_sqlite = SQLiteSingleton.getInstance();
		} 
		catch (DatabaseException e) 
		{
			System.err.println(e);
		}
	}

	@Override
	public List<Document> getAll() {
		List<Document> documents = new ArrayList<>();
		try
		{
			_stmt = _sqlite.createStatement();
			ResultSet resultSet = _stmt.executeQuery("SELECT * FROM DOCUMENT_INCOME;");
			while(resultSet.next())
			{
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String abbreviation = resultSet.getString("abbreviation");
				int requirementRegistryNumber = resultSet.getInt("require_rn");
				documents.add(new Document(id, name, abbreviation, requirementRegistryNumber));
			}
		}
		catch(SQLException e)
		{
			System.err.println(e);
		}
		return documents;
	}

	@Override
	public Document getByID(int id) {
		Document document = null;
		try 
		{
			PreparedStatement prepStmt = _sqlite.createPreparedStatement("SELECT * FROM DOCUMENT_INCOME WHERE id = ?");
			prepStmt.setInt(1, id);
			ResultSet resultSet = prepStmt.executeQuery();
			if (resultSet.isBeforeFirst()) 
			{    
				String name = resultSet.getString("name");
				String abbreviation = resultSet.getString("abbreviation");
				int requirementRegistryNumber = resultSet.getInt("require_rn");
				document = new Document(id, name, abbreviation, requirementRegistryNumber);
			}
			
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		
		return document;
	}

	@Override
	public boolean insertDocument(Document document) 
	{
		try 
		{

			String query = "INSERT INTO DOCUMENT_INCOME (name, abbreviation, require_rn) "
						 + "VALUES (?, ?, ?)";
			PreparedStatement prepStmt = _sqlite.createPreparedStatement(query);
			//prepStmt.setInt(1, document.get_id());
			prepStmt.setString(1, document.get_name());
			prepStmt.setString(2, document.get_abbreviation());
			prepStmt.setInt(3, document.get_requirementRegistryNumber());
			int res = prepStmt.executeUpdate();
			return res < 0 ? false : true;
		} 
		catch (SQLException e) 
		{
			System.err.println(e);
		}
		return false;
	}

	@Override
	public boolean updateDocument(Document document) 
	{
		try 
		{
			String query = "UPDATE DOCUMENT_INCOME SET name = ?, require_rn = ? WHERE abbreviation = ?)";
			PreparedStatement prepStmt = _sqlite.createPreparedStatement(query);
			prepStmt.setString(1, document.get_name());
			prepStmt.setInt(2, document.get_requirementRegistryNumber());
			prepStmt.setString(3, document.get_abbreviation());
			int res = prepStmt.executeUpdate();
			return res < 0 ? false : true;
		} 
		catch (SQLException e) 
		{
			System.err.println(e);
		}
		return false;
	}

	public Document getByAbbreviation(String abbreviation) {
		Document document= null;
		try
		{
			PreparedStatement prepStmt = _sqlite.createPreparedStatement("SELECT * FROM DOCUMENT_INCOME WHERE abbreviation = ?");
			prepStmt.setString(1, abbreviation);
			ResultSet resultSet = prepStmt.executeQuery();
			if (resultSet.isBeforeFirst()) 
			{
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				int requirementRegistryNumber = resultSet.getInt("require_rn");
				document = new Document(id, name, abbreviation, requirementRegistryNumber);
			}
		}
		catch(SQLException e) 
		{
			System.err.println(e);
		}
		return document;
	}

}
