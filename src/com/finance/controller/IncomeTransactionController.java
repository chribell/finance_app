package com.finance.controller;

import java.util.List;

import com.finance.db.sqlite.IncomeTransactionSQLite;
import com.finance.model.Transaction;
import com.finance.util.Utilities;

public class IncomeTransactionController 
{

private IncomeTransactionSQLite _sqlite;
	
	public IncomeTransactionController()
	{
		_sqlite = new IncomeTransactionSQLite();
	}
	
	public boolean createTransactionIncome(Transaction transaction)
	{
		return _sqlite.insertTransaction(transaction);
	}
	
	public boolean deleteTransactionIncome(int id)
	{
		return _sqlite.deleteTransaction(id);
	}
	
	public List<Transaction> getAll()
	{
		return _sqlite.getAll();
	}
	
	
	public Transaction getTransactionByID(int id)
	{
		return _sqlite.getTransaction(id);
	}
	
	public List<Transaction> getTransactionIncomeByDate(String startDate, String endDate)
	{
		return _sqlite.getByDate(Utilities.parseDate(startDate), Utilities.parseDate(endDate));
	}
	
	public int getNumberOfIncomeTransactions()
	{
		return _sqlite.getAll().size();
	}
}
