package com.finance.controller;
import java.util.List;

import com.finance.db.sqlite.DocumentOutcomeSQLite;
import com.finance.db.sqlite.SQLiteSingleton;
import com.finance.model.Document;


public class DocumentOutcomeController {
	
	private DocumentOutcomeSQLite _sqlite;
	
	public DocumentOutcomeController()
	{
		_sqlite = new DocumentOutcomeSQLite();
	}
	
	public boolean createDocumentOutcome(Document document)
	{
		return _sqlite.insertDocument(document);
	}
	
	public boolean updateDocumentOutcome(Document document)
	{
		return _sqlite.updateDocument(document);
	}
	
	public Document getDocumentOutcomeByAbbreviation(String abbreviation) 
	{
		return _sqlite.getByAbbreviation(abbreviation);
	}
	
	public List<Document> getAll()
	{
		return _sqlite.getAll();
	}

	public int getNumberOfOutcomeDocuments()
	{
		return _sqlite.getAll().size();
	}

}
