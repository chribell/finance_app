package com.finance.controller;

import java.util.List;

import com.finance.db.sqlite.CategoryIncomeSQLite;
import com.finance.db.sqlite.SQLiteSingleton;
import com.finance.model.Account;
import com.finance.model.Category;
import com.finance.model.Document;

public class CategoryIncomeController {
	
	private CategoryIncomeSQLite _sqlite;
	
	public CategoryIncomeController()
	{
		_sqlite = new CategoryIncomeSQLite();
	}
	
	public boolean createCategoryIncome(Category category)
	{
		return _sqlite.insertCategory(category);
	}
	
	public boolean updateCategoryIncome(Category category)
	{
		return _sqlite.updateCategory(category);
	}
	
	public Category getCategoryIncomeByCode(String code)
	{
		return _sqlite.getByCode(code);
	}
	
	public List<Category> getAll()
	{
		return _sqlite.getAll();
	}

	public int getNumberOfIncomeCategories()
	{
		return _sqlite.getAll().size();
	}

}
