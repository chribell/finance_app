package com.finance.controller;

import java.util.List;

import com.finance.db.sqlite.OutcomeTransactionSQLite;
import com.finance.model.Transaction;
import com.finance.util.Utilities;

public class OutcomeTranstactionController 
{
private OutcomeTransactionSQLite _sqlite;
	
	public OutcomeTranstactionController()
	{
		_sqlite = new OutcomeTransactionSQLite();
	}
	
	public boolean createTransactionOutcome(Transaction transaction)
	{
		return _sqlite.insertTransaction(transaction);
	}
	
	public boolean deleteTransactionOutcome(int id)
	{
		return _sqlite.deleteTransaction(id);
	}
	
	public List<Transaction> getAll()
	{
		return _sqlite.getAll();
	}
	
	public Transaction getTransactionByID(int id)
	{
		return _sqlite.getTransaction(id);
	}
	
	public List<Transaction> getTransactionIncomeByDate(String startDate, String endDate)
	{
		return _sqlite.getByDate(Utilities.parseDate(startDate), Utilities.parseDate(endDate));
	}
	
	public int getNumberOfOutcomeTransactions()
	{
		return _sqlite.getAll().size();
	}

}
