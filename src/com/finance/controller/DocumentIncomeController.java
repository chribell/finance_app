package com.finance.controller;
import java.util.List;

import com.finance.db.sqlite.DocumentIncomeSQLite;
import com.finance.db.sqlite.SQLiteSingleton;
import com.finance.model.Account;
import com.finance.model.Document;


public class DocumentIncomeController {
	
	private DocumentIncomeSQLite _sqlite;
	
	public DocumentIncomeController()
	{
		_sqlite = new DocumentIncomeSQLite();
	}
	
	public boolean createDocumentIncome(Document document)
	{
		return _sqlite.insertDocument(document);
	}
	
	public boolean updateDocumentIncome(Document document)
	{
		return _sqlite.updateDocument(document);
	}

	public Document getDocumentIncomeByAbbreviation(String abbreviation) 
	{
		return _sqlite.getByAbbreviation(abbreviation);
	}
	
	public List<Document> getAll()
	{
		return _sqlite.getAll();
	}
	
	public int getNumberOfIncomeDocuments()
	{
		return _sqlite.getAll().size();
	}

	

}
