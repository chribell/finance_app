package com.finance.controller;

import java.util.List;

import com.finance.db.sqlite.ClientSQLite;
import com.finance.db.sqlite.SQLiteSingleton;
import com.finance.model.Account;

public class ClientController 
{
	
	private ClientSQLite _sqlite;
	
	public ClientController()
	{
		_sqlite = new ClientSQLite();
	}
	
	public boolean createClient(Account account)
	{
		return _sqlite.insertAccount(account);
	}
	
	public boolean updateClient(Account account)
	{
		return _sqlite.updateAccount(account);
	}
	
	public boolean deleteteClient(int id)
	{
		return _sqlite.deleteAccount(id);
	}
	
	public Account getClientByCode(String code)
	{
		return _sqlite.getByCode(code);
	}
	
	public List<Account> getAll()
	{
		return _sqlite.getAll();
	}
	
	public int getNumberOfClients()
	{
		return _sqlite.getAll().size();
	}

}
