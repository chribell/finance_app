package com.finance.controller;

import java.util.List;

import com.finance.db.sqlite.SQLiteSingleton;
import com.finance.db.sqlite.SupplierSQLite;
import com.finance.model.Account;

public class SupplierController {
	
	private SupplierSQLite _sqlite;
	
	public SupplierController()
	{
		_sqlite = new SupplierSQLite();
	}
	
	public boolean createSupplier(Account account)
	{
		return _sqlite.insertAccount(account);
	}
	
	public boolean updateSupplier(Account account)
	{
		return _sqlite.updateAccount(account);
	}
	
	public boolean deleteteSupplier(int id)
	{
		return _sqlite.deleteAccount(id);
	}
	
	public Account getSupplierByCode(String code) 
	{
		return _sqlite.getByCode(code);
	}
	
	public List<Account> getAll()
	{
		return _sqlite.getAll();
	}

	public int getNumberOfSuppliers()
	{
		return _sqlite.getAll().size();
	}
}
