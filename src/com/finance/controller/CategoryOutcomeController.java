package com.finance.controller;

import java.util.List;

import com.finance.db.sqlite.CategoryOutcomeSQLite;
import com.finance.db.sqlite.SQLiteSingleton;
import com.finance.model.Category;

public class CategoryOutcomeController {
	
	private CategoryOutcomeSQLite _sqlite;
	
	public CategoryOutcomeController()
	{
		_sqlite = new CategoryOutcomeSQLite();
	}
	
	public boolean createCategoryOutcome(Category category)
	{
		return _sqlite.insertCategory(category);
	}
	
	public boolean updateCategoryOutcome(Category category)
	{
		return _sqlite.updateCategory(category);
	}
	
	public Category getCategoryOutcomeByCode(String code)
	{
		return _sqlite.getByCode(code);
	}
	
	public List<Category> getAll()
	{
		return _sqlite.getAll();
	}

	public int getNumberOfOutcomeCategories()
	{
		return _sqlite.getAll().size();
	}

}
