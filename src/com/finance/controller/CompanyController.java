package com.finance.controller;

import com.finance.db.DatabaseException;
import com.finance.db.sqlite.CompanySQLite;
import com.finance.db.sqlite.SQLiteSingleton;
import com.finance.model.Company;

public class CompanyController 
{
	private CompanySQLite _sqlite;
	
	public CompanyController()
	{
		_sqlite = new CompanySQLite();
	}
	
	public boolean createCompany(Company company)
	{
		return _sqlite.insertCompany(company);
	}
	
	public boolean updateCompany(Company company)
	{
		return _sqlite.updateCompany(company);
	}
	
	public int getNumberOfCompanies()
	{
		return _sqlite.getAll().size();
	}

	public Company getLastCompany()
	{
		return _sqlite.getLastCompany();
	}
	
	public static void createCompanyDB(String dbName)
	{
		SQLiteSingleton.setDatabasePath(dbName);
		try 
		{
			SQLiteSingleton.getInstance();
		} 
		catch (DatabaseException e) 
		{
			System.err.println(e);
		}
	}
	
	public static void selectCompanyDB(String dbPath)
	{
		SQLiteSingleton.setDatabasePath(dbPath);
		try 
		{
			SQLiteSingleton.getInstance();
		} 
		catch (DatabaseException e) 
		{
			
		}
	}
}
