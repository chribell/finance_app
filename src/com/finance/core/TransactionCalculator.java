package com.finance.core;

import java.util.List;

import com.finance.model.Transaction;

public class TransactionCalculator 
{
	private List<Transaction> _transactions;
	private double[] _taxes;
	
	private double _smallAmountSum; 
	private double _smallTaxSum;
	private double _largeAmountSum;
	private double _largeTaxSum;
	private double _totalAmount;
	private double _totalTax;
	
	
	public TransactionCalculator(List<Transaction> transactions, double[] taxes)
	{
		_transactions = transactions;
		_taxes = taxes;
		_smallAmountSum = 0.0;
		_smallTaxSum = 0.0;
		_largeAmountSum = 0.0;
		_largeTaxSum = 0.0;
		_totalAmount = 0.0;
		_totalTax = 0.0;
	}
	
	
	public void calculate()
	{
		for(Transaction transaction : _transactions)
		{
			int transactionType = transaction.getTypeID();
			if(transactionType == 1 || transactionType == 3 )
			{
				_smallAmountSum += transaction.getAmount();
				_smallTaxSum += ((_taxes[0] / 100) * transaction.getAmount());
			}
			else
			{
				_largeAmountSum += transaction.getAmount();
				_largeTaxSum += ((_taxes[1] / 100) * transaction.getAmount());
			}
		}
		
		_totalAmount = _smallAmountSum + _largeAmountSum;
		_totalTax = _smallTaxSum + _largeTaxSum;
	}

	public double get_smallAmountSum() {
		return _smallAmountSum;
	}


	public double get_smallTaxSum() {
		return _smallTaxSum;
	}


	public double get_largeAmountSum() {
		return _largeAmountSum;
	}


	public double get_largeTaxSum() {
		return _largeTaxSum;
	}


	public double get_totalAmount() {
		return _totalAmount;
	}


	public double get_totalTax() {
		return _totalTax;
	}
}
