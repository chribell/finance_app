package com.finance.model;

public class TransactionType 
{
	private int _id;
	private String _typeName;
	private double _taxFactor;
	
	public TransactionType(int id, String name, double tax)
	{
		_id = id;
		_typeName = name;
		_taxFactor = tax;
	}
	
	public int get_id()
	{
		return _id;
	}
	
	public String get_name()
	{
		return _typeName;
	}
	
	public double get_taxFactor() 
	{
		return _taxFactor;
	}
	
	@Override
	public String toString()
	{
		return _typeName + " " + _taxFactor + "%";
	}
}
