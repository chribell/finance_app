package com.finance.model;

public class Category {
	
	private int _id;
	private String _code;
	private String _explanation;
	private int _numOfFields;
	
	public Category(int id, String code, String explanation, int numOfFields) 
	{
		_id = id;
		_code = code;
		_explanation = explanation;
		_numOfFields = numOfFields;
	}
	
	public int get_id() 
	{
		return _id;
	}

	public String get_code() 
	{
		return _code;
	}

	public String get_explanation() 
	{
		return _explanation;
	}

	public int get_numberOfFields() 
	{
		return _numOfFields;
	}

	public String[] getFields()
	{
		return new String[] {Integer.toString(_id), _code, _explanation, Integer.toString(_numOfFields)};
	}
	
	@Override
	public String toString()
	{
		return _code;
	}
}
