package com.finance.model;

public class Account {
	
	private int _id;
	private String _code;
	private String _name;
	private String _inCharge;
	private String _occupation;
	private String _city;
	private String _zipCode;
	private String _registryNumber;
	private String _taxStore;
	private String _address;
	private String _phone;
	
	public Account(int _id, String _code, String _name, String _inCharge, String _occupation, String _city,
			String _zipCode, String _registryNumber, String _taxStore,
			String _address, String _phone) {

		this._id = _id;
		this._code = _code;
		this._name = _name;
		this._inCharge = _inCharge;
		this._occupation = _occupation;
		this._city = _city;
		this._zipCode = _zipCode;
		this._registryNumber = _registryNumber;
		this._taxStore = _taxStore;
		this._address = _address;
		this._phone = _phone;
	}

	public int get_id() {
		return _id;
	}
	
	public String get_code() {
		return _code;
	}

	public String get_name() {
		return _name;
	}
	
	public String get_inCharge() {
		return _inCharge;
	}

	public String get_occupation() {
		return _occupation;
	}

	public String get_city() {
		return _city;
	}

	public String get_zipCode() {
		return _zipCode;
	}

	public String get_registryNumber() {
		return _registryNumber;
	}

	public String get_taxStore() {
		return _taxStore;
	}

	public String get_address() {
		return _address;
	}

	public String get_phone() {
		return _phone;
	}
	
	public String[] getFields()
	{
		return new String[] {Integer.toString(_id), _code, _name, _inCharge, _occupation, _city, _zipCode, _registryNumber, _taxStore,
							_address, _phone};
	}
	
	@Override 
	public String toString()
	{
		return _code;
	}

}
