package com.finance.model;

import com.finance.util.Utilities;

public class Transaction {
	
	private int _id;
	private String _date;
	private Category _category;
	private Account _account;
	private Document _document;
	private int _typeID;
	private double _amount;
	
	public Transaction(int id, int typeID, String date, Category category, Document document, double amount) 
	{
		_id = id;
		_typeID = typeID;
		_date = date;
		_category = category;
		_document = document;
		_amount = amount;
	}

	public void setAccount(Account account)
	{
		_account = account; 
	}
	
	public int getTypeID()
	{
		return _typeID;
	}
	
	public int getID()
	{
		return _id;
	}

	public Category getCategory() 
	{
		return _category;
	}
	
	public Account getAccount()
	{
		return _account;
	}
	
	public String getDate()
	{
		return _date;
	}
	
	public Document getDocument()
	{
		return _document;
	}

	public double getAmount() 
	{
		return _amount;
	}
	
	public String[] getFields()
	{
		return new String[] {Integer.toString(_id), Integer.toString(_typeID), Utilities.parseReverseDate(_date), _category.get_code(), _document.get_abbreviation(), Double.toString(_amount)};
	}

}
