package com.finance.model;

public class Document {
	
	private int _id;
	private String _name;
	private String _abbreviation;
	private int _requirementRegistryNumber;
	
	public Document(int _id, String _name, String _abbreviation,
			int _requirementRegistryNumber) {

		this._id = _id;
		this._name = _name;
		this._abbreviation = _abbreviation;
		this._requirementRegistryNumber = _requirementRegistryNumber;
	}

	public int get_id() {
		return _id;
	}

	public String get_name() {
		return _name;
	}

	public String get_abbreviation() {
		return _abbreviation;
	}

	public int get_requirementRegistryNumber() {
		return _requirementRegistryNumber;
	}
	
	@Override
	public String toString()
	{
		return _abbreviation;
	}
	
	public String[] getFields()
	{
		return new String[] {Integer.toString(_id), _name, _abbreviation, Integer.toString(_requirementRegistryNumber)};
	}

}
