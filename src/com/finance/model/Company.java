package com.finance.model;

public class Company {
	
	private int _id;
	private String _name;
	private String _registryNumber;
	private String _expertise;
	private String _city;
	private String _address;
	private String _fax;
	private String _title;
	private String _taxStore;
	private String _email;
	private String _zipCode;
	private String _phone;
	private String _startDate;
	
	public Company(int _id, String _name, String _registryNumber, String _expertise,
			String _city, String _address, String _fax,
			String _title, String _taxStore, String _email,
			String _zipCode, String _phone, String _startDate) {

		this._id = _id;
		this._name = _name;
		this._registryNumber = _registryNumber;
		this._expertise = _expertise;
		this._city = _city;
		this._address = _address;
		this._fax = _fax;
		this._title = _title;
		this._taxStore = _taxStore;
		this._email = _email;
		this._zipCode = _zipCode;
		this._phone = _phone;
		this._startDate = _startDate;
	}
	
	public int get_id() {
		return _id;
	}

	public String get_name() {
		return _name;
	}

	public String get_registryNumber() {
		return _registryNumber;
	}

	public String get_expertise() {
		return _expertise;
	}

	public String get_city() {
		return _city;
	}

	public String get_address() {
		return _address;
	}

	public String get_fax() {
		return _fax;
	}

	public String get_title() {
		return _title;
	}

	public String get_taxStore() {
		return _taxStore;
	}

	public String get_email() {
		return _email;
	}

	public String get_zipCode() {
		return _zipCode;
	}

	public String get_phone() {
		return _phone;
	}

	public String get_startDate() {
		return _startDate;
	}
	
	

}
